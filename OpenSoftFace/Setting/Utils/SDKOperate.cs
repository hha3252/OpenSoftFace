using OpenSoftFace.Setting.Entity;
using OpenSoftFace.Setting.Models;
using OpenSoftFace.Setting.Common;
using LogUtil;
using Model.Entity;
using System;
using System.Drawing;
using System.Globalization;
using System.Runtime.InteropServices;
using Utils;

namespace OpenSoftFace.Setting.Utils
{
	public class SDKOperate
	{
		private static bool engineInitStatus;

		private static IntPtr pImageEngine;

		public static bool ActiveStatus;

		private static IniHelper iniHelper;

		public static string DeadlineDate;

		public static string SDKVersion;

		static SDKOperate()
		{
			SDKOperate.engineInitStatus = false;
			SDKOperate.pImageEngine = IntPtr.Zero;
			SDKOperate.ActiveStatus = false;
			SDKOperate.iniHelper = new IniHelper("setting.ini", "setting");
			SDKOperate.DeadlineDate = "--";
			SDKOperate.SDKVersion = string.Empty;
			SDKOperate.activeSDK();
			bool activeStatus = SDKOperate.ActiveStatus;
			if (activeStatus)
			{
				SDKOperate.initEngine();
				SDKOperate.GetVersion();
			}
		}

		private static void activeSDK()
		{
			try
			{
				string appId = string.Empty;
				string sdkKey = string.Empty;
				bool flag = SDKOperate.iniHelper.Dict.ContainsKey("APPID");
				if (flag)
				{
					appId = SDKOperate.iniHelper.Dict["APPID"];
				}
				bool flag2 = SDKOperate.iniHelper.Dict.ContainsKey("SDKKEY");
				if (flag2)
				{
					sdkKey = SDKOperate.iniHelper.Dict["SDKKEY"];
				}
				int num = ASFFunctions.ASFActivation(appId, sdkKey);
				bool flag3 = num == 0 || num == 90114;
				if (flag3)
				{
					SDKOperate.ActiveStatus = true;
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(SDKOperate), se);
			}
		}

		private static void initEngine()
		{
			try
			{
				DetectionMode detectMode = (DetectionMode)4294967295u;
				ASF_OrientPriority detectFaceOrientPriority = ASF_OrientPriority.ASF_OP_ALL_OUT;
				int detectFaceScaleVal = 16;
				int detectFaceMaxNum = 5;
				int combinedMask = 37;
				int num = ASFFunctions.ASFInitEngine(detectMode, detectFaceOrientPriority, detectFaceScaleVal, detectFaceMaxNum, combinedMask, ref SDKOperate.pImageEngine);
				bool flag = num == 0;
				if (flag)
				{
					SDKOperate.engineInitStatus = true;
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(SDKOperate), se);
			}
		}

		public static void UninitEngine()
		{
			try
			{
				bool flag = SDKOperate.pImageEngine != IntPtr.Zero;
				if (flag)
				{
					ASFFunctions.ASFUninitEngine(SDKOperate.pImageEngine);
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(SDKOperate), se);
			}
		}

		private static void GetVersion()
		{
			try
			{
				IntPtr intPtr = MemoryUtil.Malloc(MemoryUtil.SizeOf<ASF_ActiveFileInfo>());
				int num = ASFFunctions.ASFGetActiveFileInfo(intPtr);
				bool flag = num == 0;
				if (flag)
				{
					ASF_ActiveFileInfo aSF_ActiveFileInfo = default(ASF_ActiveFileInfo);
					aSF_ActiveFileInfo = MemoryUtil.PtrToStructure<ASF_ActiveFileInfo>(intPtr);
					string text = Marshal.PtrToStringAnsi(aSF_ActiveFileInfo.endTime);
					bool flag2 = !string.IsNullOrEmpty(text);
					if (flag2)
					{
						SDKOperate.DeadlineDate = DateTimeUtil.ToDateTime(text).ToString("yyyy/MM/dd", DateTimeFormatInfo.InvariantInfo);
					}
				}
				ASF_VERSION aSF_VERSION = ASFFunctions.ASFGetVersion();
				string text2 = Marshal.PtrToStringAnsi(aSF_VERSION.Version);
				bool flag3 = !string.IsNullOrEmpty(text2) && text2.Length >= 3;
				if (flag3)
				{
					SDKOperate.SDKVersion = text2.Substring(0, 3);
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(SDKOperate), se);
			}
		}

		public static FeatureInfo DetectAndRecognitionImage(string filePath, ref string message, ref Image cutImage)
		{
			FeatureInfo result;
			try
			{
				bool flag = !SDKOperate.ActiveStatus;
				if (flag)
				{
					message = "SDK激活失败,请确认后重试!";
					result = null;
					return result;
				}
				bool flag2 = !SDKOperate.engineInitStatus;
				if (flag2)
				{
					message = "引擎初始化失败,注册照人脸检测失败!";
					result = null;
					return result;
				}
				FeatureInfo featureInfo = new FeatureInfo();
				Image image = ImageUtil.readFromFile(filePath);
				bool flag3 = image == null;
				if (flag3)
				{
					message = "图片读取失败!";
					result = null;
					return result;
				}
				bool flag4 = image.Width % 4 != 0;
				if (flag4)
				{
					image = ImageUtil.ScaleImage(image, image.Width - image.Width % 4, image.Height);
				}
				ImageInfo imageInfo = ImageUtil.ReadBMP(image);
				bool flag5 = imageInfo == null;
				if (flag5)
				{
					message = "图片数据读取失败!";
					result = null;
					return result;
				}
				ASF_MultiFaceInfo aSF_MultiFaceInfo = FaceUtil.DetectFace(SDKOperate.pImageEngine, imageInfo);
				bool flag6 = aSF_MultiFaceInfo.faceNum != 1;
				if (flag6)
				{
					message = ((aSF_MultiFaceInfo.faceNum <= 0) ? "未检测到人脸，请重新上传!" : "请上传单人脸照片!");
					MemoryUtil.Free(imageInfo.imgData);
					result = null;
					return result;
				}
				MRECT mRECT = MemoryUtil.PtrToStructure<MRECT>(aSF_MultiFaceInfo.faceRects);
				int left = mRECT.left;
				int right = mRECT.right;
				int top = mRECT.top;
				int bottom = mRECT.bottom;
				cutImage = ImageUtil.CutImage(image, mRECT.left, mRECT.top, mRECT.right, mRECT.bottom, ref left, ref right, ref top, ref bottom);
				bool flag7 = cutImage == null;
				if (flag7)
				{
					message = "人脸裁剪失败!";
					MemoryUtil.Free(imageInfo.imgData);
					result = null;
					return result;
				}
				featureInfo.FLeft = left;
				featureInfo.FRight = right;
				featureInfo.FTop = top;
				featureInfo.FBottom = bottom;
				ASF_SingleFaceInfo aSF_SingleFaceInfo = default(ASF_SingleFaceInfo);
				int num = -1;
				ASF_FaceFeature aSF_FaceFeature = FaceUtil.ExtractFeature(SDKOperate.pImageEngine, imageInfo, aSF_MultiFaceInfo, out aSF_SingleFaceInfo, ref num);
				bool flag8 = num != 0;
				if (flag8)
				{
					message = "图片质量不合格，请重新上传!";
					MemoryUtil.Free(imageInfo.imgData);
					result = null;
					return result;
				}
				featureInfo.FOrient = aSF_SingleFaceInfo.faceOrient;
				featureInfo.Feature = new byte[aSF_FaceFeature.featureSize];
				MemoryUtil.Copy(aSF_FaceFeature.feature, featureInfo.Feature, 0, aSF_FaceFeature.featureSize);
				featureInfo.FeatureSize = aSF_FaceFeature.featureSize;
				MemoryUtil.Free(imageInfo.imgData);
				result = featureInfo;
				return result;
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(SDKOperate), se);
			}
			result = null;
			return result;
		}
	}
}
