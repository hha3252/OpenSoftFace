using OpenSoftFace.Setting.Entity;
using OpenSoftFace.Setting.Models;
using System;

namespace OpenSoftFace.Setting.Utils
{
	public class FaceUtil
	{
		public static ASF_MultiFaceInfo DetectFace(IntPtr pEngine, ImageInfo imageInfo)
		{
			ASF_MultiFaceInfo aSF_MultiFaceInfo = default(ASF_MultiFaceInfo);
			IntPtr intPtr = MemoryUtil.Malloc(MemoryUtil.SizeOf<ASF_MultiFaceInfo>());
			int num = ASFFunctions.ASFDetectFaces(pEngine, imageInfo.width, imageInfo.height, imageInfo.format, imageInfo.imgData, intPtr, ASF_DetectModel.ASF_DETECT_MODEL_RGB);
			bool flag = num != 0;
			ASF_MultiFaceInfo result;
			if (flag)
			{
				MemoryUtil.Free(intPtr);
				result = aSF_MultiFaceInfo;
			}
			else
			{
				aSF_MultiFaceInfo = MemoryUtil.PtrToStructure<ASF_MultiFaceInfo>(intPtr);
				MemoryUtil.Free(intPtr);
				result = aSF_MultiFaceInfo;
			}
			return result;
		}

		public static ASF_FaceFeature ExtractFeature(IntPtr pEngine, ImageInfo imageInfo, ASF_MultiFaceInfo multiFaceInfo, out ASF_SingleFaceInfo singleFaceInfo, ref int featureRetCode)
		{
			singleFaceInfo = default(ASF_SingleFaceInfo);
			singleFaceInfo.faceRect = MemoryUtil.PtrToStructure<MRECT>(multiFaceInfo.faceRects);
			singleFaceInfo.faceOrient = MemoryUtil.PtrToStructure<int>(multiFaceInfo.faceOrients);
			IntPtr intPtr = MemoryUtil.Malloc(MemoryUtil.SizeOf<ASF_SingleFaceInfo>());
			MemoryUtil.StructureToPtr<ASF_SingleFaceInfo>(singleFaceInfo, intPtr);
			IntPtr intPtr2 = MemoryUtil.Malloc(MemoryUtil.SizeOf<ASF_FaceFeature>());
			featureRetCode = ASFFunctions.ASFFaceFeatureExtract(pEngine, imageInfo.width, imageInfo.height, imageInfo.format, imageInfo.imgData, intPtr, intPtr2);
			Console.WriteLine("FR Extract Feature result:" + featureRetCode);
			bool flag = featureRetCode != 0;
			ASF_FaceFeature result;
			if (flag)
			{
				MemoryUtil.Free(intPtr);
				MemoryUtil.Free(intPtr2);
				result = default(ASF_FaceFeature);
			}
			else
			{
				ASF_FaceFeature aSF_FaceFeature = MemoryUtil.PtrToStructure<ASF_FaceFeature>(intPtr2);
				MemoryUtil.Free(intPtr);
				MemoryUtil.Free(intPtr2);
				result = aSF_FaceFeature;
			}
			return result;
		}
	}
}
