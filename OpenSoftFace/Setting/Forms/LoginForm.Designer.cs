﻿using OpenSoftFace.Setting.Controls;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace OpenSoftFace.Setting.Forms
{
    partial class LoginForm
	{
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			bool flag = disposing && this.components != null;
			if (flag)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.btn_login = new System.Windows.Forms.Button();
            this.panel = new System.Windows.Forms.Panel();
            this.txt_user = new OpenSoftFace.Setting.Controls.MyBorderTextBoxcs();
            this.txt_password = new OpenSoftFace.Setting.Controls.MyBorderTextBoxcs();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_hide)).BeginInit();
            this.panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.BackColor = System.Drawing.Color.White;
            this.splitContainer.Panel2.Controls.Add(this.panel);
            this.splitContainer.Size = new System.Drawing.Size(423, 257);
            this.splitContainer.SplitterDistance = 73;
            this.splitContainer.TabStop = false;
            // 
            // btn_close
            // 
            this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btn_close.Image = ((System.Drawing.Image)(resources.GetObject("btn_close.Image")));
            this.btn_close.Location = new System.Drawing.Point(385, 21);
            this.btn_close.Size = new System.Drawing.Size(12, 12);
            // 
            // lbl_title
            // 
            this.lbl_title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.lbl_title.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.lbl_title.Size = new System.Drawing.Size(423, 73);
            this.lbl_title.Text = "人脸考勤系统";
            // 
            // btn_hide
            // 
            this.btn_hide.Location = new System.Drawing.Point(66, 12);
            this.btn_hide.Size = new System.Drawing.Size(12, 12);
            // 
            // btn_login
            // 
            this.btn_login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_login.FlatAppearance.BorderSize = 0;
            this.btn_login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_login.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btn_login.ForeColor = System.Drawing.Color.White;
            this.btn_login.Location = new System.Drawing.Point(65, 125);
            this.btn_login.Name = "btn_login";
            this.btn_login.Size = new System.Drawing.Size(292, 43);
            this.btn_login.TabIndex = 1;
            this.btn_login.Text = "确 认";
            this.btn_login.UseVisualStyleBackColor = false;
            this.btn_login.Click += new System.EventHandler(this.login_btn_Click);
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(247)))), ((int)(((byte)(247)))));
            this.panel.Controls.Add(this.txt_user);
            this.panel.Controls.Add(this.txt_password);
            this.panel.Controls.Add(this.btn_login);
            this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(423, 180);
            this.panel.TabIndex = 0;
            // 
            // txt_user
            // 
            this.txt_user.BackColor = System.Drawing.Color.White;
            this.txt_user.BackImage = null;
            this.txt_user.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            this.txt_user.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_user.BorderWidth = 1;
            this.txt_user.EmptyColor = System.Drawing.Color.DarkGray;
            this.txt_user.EmptyText = "请输入管理员账号";
            this.txt_user.FontSize = 12F;
            this.txt_user.IsPassWord = true;
            this.txt_user.Location = new System.Drawing.Point(66, 21);
            this.txt_user.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_user.MaxLength = 20;
            this.txt_user.Multiline = false;
            this.txt_user.Name = "txt_user";
            this.txt_user.Padding = new System.Windows.Forms.Padding(5, 8, 5, 0);
            this.txt_user.Size = new System.Drawing.Size(291, 41);
            this.txt_user.TabIndex = 2;
            // 
            // txt_password
            // 
            this.txt_password.BackColor = System.Drawing.Color.White;
            this.txt_password.BackImage = null;
            this.txt_password.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            this.txt_password.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_password.BorderWidth = 1;
            this.txt_password.EmptyColor = System.Drawing.Color.DarkGray;
            this.txt_password.EmptyText = "请输入管理员密码";
            this.txt_password.FontSize = 12F;
            this.txt_password.IsPassWord = true;
            this.txt_password.Location = new System.Drawing.Point(65, 77);
            this.txt_password.MaxLength = 20;
            this.txt_password.Multiline = false;
            this.txt_password.Name = "txt_password";
            this.txt_password.Padding = new System.Windows.Forms.Padding(5, 7, 5, 0);
            this.txt_password.Size = new System.Drawing.Size(292, 38);
            this.txt_password.TabIndex = 0;
            // 
            // LoginForm
            // 
            this.ClientSize = new System.Drawing.Size(423, 257);
            this.KeyPreview = true;
            this.Name = "LoginForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ArcOfficeSetting";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.LoginForm_Load);
            this.Shown += new System.EventHandler(this.LoginForm_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LoginForm_KeyDown);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_hide)).EndInit();
            this.panel.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        private Button btn_login;
        private Panel panel;
        private MyBorderTextBoxcs txt_password;
        private MyBorderTextBoxcs txt_user;
    }
}