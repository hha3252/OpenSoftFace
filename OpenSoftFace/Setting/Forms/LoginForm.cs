using OpenSoftFace.Setting.Controls;
using OpenSoftFace.Setting.Forms.SettingForms;
using LogUtil;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace OpenSoftFace.Setting.Forms
{
	public partial class LoginForm : DragBaseForm
	{
		
		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams createParams = base.CreateParams;
				createParams.ExStyle |= 33554432;
				return createParams;
			}
		}

		public LoginForm()
		{
			this.InitializeComponent();
            BaseForm baseForm = new BaseForm();
            baseForm.Disposed += delegate
            {
                base.Close();
            };
            base.Hide();
            baseForm.ShowDialog();
		}

		private void LoginForm_KeyDown(object sender, KeyEventArgs e)
		{
			try
			{
				bool flag = e.KeyCode == Keys.Return;
				if (flag)
				{
					bool flag2 = string.IsNullOrEmpty(this.txt_password.Text);
					if (flag2)
					{
						this.txt_password.Focus();
					}
					else
					{
						this.login_btn_Click(sender, e);
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void login_btn_Click(object sender, EventArgs e)
		{
			try
			{
				string value = this.txt_password.Text.Trim();
				bool flag = string.IsNullOrEmpty(value);
				if (flag)
				{
					ErrorDialogForm.ShowErrorMsg("管理员密码不能为空!", "", false, null, false);
				}
				else
				{
					bool flag2 = "admin".Equals(value);
					if (flag2)
					{
						BaseForm baseForm = new BaseForm();
						baseForm.Disposed += delegate
						{
							base.Close();
						};
						base.Hide();
						baseForm.ShowDialog();
					}
					else
					{
						ErrorDialogForm.ShowErrorMsg("管理员密码错误!", "", false, null, false);
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void LoginForm_Shown(object sender, EventArgs e)
		{
		}

		private void LoginForm_Load(object sender, EventArgs e)
		{
		}



		
	}
}
