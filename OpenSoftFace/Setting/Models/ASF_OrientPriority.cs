using System;

namespace OpenSoftFace.Setting.Models
{
	public enum ASF_OrientPriority
	{
		ASF_OP_0_ONLY = 1,
		ASF_OP_90_ONLY,
		ASF_OP_270_ONLY,
		ASF_OP_180_ONLY,
		ASF_OP_ALL_OUT
	}
}
