using System;

namespace OpenSoftFace.Setting.Models
{
	public struct ASF_FaceFeature
	{
		public IntPtr feature;

		public int featureSize;
	}
}
