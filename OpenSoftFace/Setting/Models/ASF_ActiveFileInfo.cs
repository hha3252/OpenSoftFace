using System;

namespace OpenSoftFace.Setting.Models
{
	public struct ASF_ActiveFileInfo
	{
		public IntPtr startTime;

		public IntPtr endTime;

		public IntPtr activeKey;

		public IntPtr platform;

		public IntPtr sdkType;

		public IntPtr appId;

		public IntPtr sdkKey;

		public IntPtr sdkVersion;

		public IntPtr fileVersion;
	}
}
