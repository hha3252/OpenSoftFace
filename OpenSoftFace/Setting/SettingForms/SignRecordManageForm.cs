using OpenSoftFace.Setting.Common;
using OpenSoftFace.Setting.Controls;
using DAL.Service;
using DAL.Service.Impl;
using LogUtil;
using Model.Common;
using Model.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Utils;
using OpenSoftFace.WebService;

namespace OpenSoftFace.Setting.Forms.SettingForms
{
	public class SignRecordManageForm : Form
	{
		private IMainInfoService personInfoService;

		private IAttendanceRecordService attendanceRecordService;

		private BaseForm baseForm;

		private string lastDepartmentId = string.Empty;

		private string dbtEditName = "detail";

		private StringBuilder sqlBuilder = new StringBuilder();

		private const string orderBy = " id desc ";

		private List<MainInfo> recordList;

		private Dictionary<int, MainInfo> recordMap = new Dictionary<int, MainInfo>();

		private int offset;

		private DateTime search_start;

		private DateTime search_end;

		private DateTime tempDateTime = DateTime.Now;

		private IContainer components = null;

		private MyTableLayoutPanel tableLayoutPanel;

		private MyButton btn_export;

		private SplitContainer splitContainer;

		private Label lbl_name;

		private MySplitDataGridView recordDataGridView;

		private DateTimePicker date_time_start;

		private Label lab_tip;

		private DateTimePicker date_time_end;

		private MyButton btn_search;

		private MyBoderPanel boderPanel;

		private MyTextBox mytxt_name;

		[method: CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
		public event Action ShowLayer;

		[method: CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
		public event Action HideLayer;

		public SignRecordManageForm()
		{
			this.InitializeComponent();
		}

		public SignRecordManageForm(BaseForm baseForm)
		{
			this.InitializeComponent();
			this.baseForm = baseForm;
			base.TopMost = true;
		}

		private void SignRecordManageForm_Load(object sender, EventArgs e)
		{
			try
			{
				this.date_time_start.Value = DateTime.Now.AddDays((double)(1 - DateTime.Now.Day));
				this.date_time_end.Value = DateTime.Now;
				this.recordDataGridView.PageSize = 10;
				this.personInfoService = new MainInfoServiceImpl();
				this.attendanceRecordService = new AttendanceRecordServiceImpl();
				this.sqlBuilder = new StringBuilder();
				Dictionary<string, int> dictValue = new Dictionary<string, int>
				{
					{
						"序号",
						80
					},
					{
						"姓名",
						0
					},
					{
						"编号",
						0
					},
					{
						"部门",
						0
					},
					{
						"出勤天数",
						0
					}
				};
				this.recordDataGridView.CreateListHeader(dictValue, 0);
				DataGridViewTextBoxColumn dataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
				dataGridViewTextBoxColumn.CellTemplate.Value = "详情";
				dataGridViewTextBoxColumn.Name = this.dbtEditName;
				dataGridViewTextBoxColumn.HeaderText = "操作";
				dataGridViewTextBoxColumn.DefaultCellStyle.NullValue = "详情";
				dataGridViewTextBoxColumn.Width = 60;
				dataGridViewTextBoxColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
				this.recordDataGridView.AddButton(dataGridViewTextBoxColumn, DataGridViewAutoSizeColumnMode.None);
				this.recordDataGridView.HideCheckBox();
				this.Reload();
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public void Reload()
		{
			try
			{
				Action expr_08 = this.ShowLayer;
				if (expr_08 != null)
				{
					expr_08();
				}
				ThreadPool.QueueUserWorkItem(new WaitCallback(delegate
				{
					this.InitDataGridView(true);
					base.Invoke(new Action(delegate
					{
						Action expr_07 = this.HideLayer;
						if (expr_07 != null)
						{
							expr_07();
						}
					}));
				}));
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void InitDataGridView(bool isInvoke = false)
		{
			try
			{
				string text = string.Format("((p_valid != -1 and datetime(add_time) <=datetime('{1} 23:59:59')) or (p_valid = -1 and datetime(add_time) <=datetime('{1} 23:59:59') and datetime(update_time) >=datetime('{0} 00:00:00')))", DateTimeUtil.getMonthFirstDay(this.date_time_start.Value).ToString("yyyy-MM-dd"), DateTimeUtil.getMonthLastDay(this.date_time_end.Value).ToString("yyyy-MM-dd"));
				this.recordList = this.personInfoService.QueryForList(new string[]
				{
					text
				}, null, null, " id desc ", 0, this.recordDataGridView.PageSize);
				this.addDataGridItem(this.recordList, true, 0, isInvoke);
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public void addDataGridItem(List<MainInfo> personList, bool isReset, int offset = 0, bool isInvoke = false)
		{
			try
			{
				string[] where = new string[5];
				string value = string.Format("((p_valid != -1 and datetime(add_time) <=datetime('{1} 23:59:59')) or (p_valid = -1 and datetime(add_time) <=datetime('{1} 23:59:59') and datetime(update_time) >=datetime('{0} 00:00:00')))", DateTimeUtil.getMonthFirstDay(this.date_time_start.Value).ToString("yyyy-MM-dd"), DateTimeUtil.getMonthLastDay(this.date_time_end.Value).ToString("yyyy-MM-dd"));
				bool flag = string.IsNullOrEmpty(this.sqlBuilder.ToString());
				if (flag)
				{
					this.sqlBuilder.Append(value);
				}
				else
				{
					bool flag2 = this.sqlBuilder.ToString().IndexOf(value) == -1;
					if (flag2)
					{
						this.sqlBuilder.Append(",").Append(value);
					}
				}
				where = this.sqlBuilder.ToString().Split(new char[]
				{
					','
				});
				int dataSize = this.personInfoService.Count(where);
				this.getAttendRecord2List(ref personList);
				int size = personList.Count;
				if (isInvoke)
				{
					base.Invoke(new Action(delegate
					{
						this.recordDataGridView.ClearDataGridView();
						this.recordDataGridView.AllSize = dataSize;
						this.recordDataGridView.Calculation(isReset);
						for (int j = 0; j < size; j++)
						{
							string[] value3 = new string[]
							{
								string.Concat(offset + j + 1),
								StringUtil.CheckStringEmpty(personList[j].PersonName),
								StringUtil.CheckStringEmpty(personList[j].PersonSerial),
								StringUtil.CheckStringEmpty(personList[j].Dept),
								personList[j].MonthAttendTotal
							};
							this.recordDataGridView.AddRowData(value3, personList[j].ID.ToString(), null);
						}
					}));
				}
				else
				{
					this.recordDataGridView.ClearDataGridView();
					this.recordDataGridView.AllSize = dataSize;
					this.recordDataGridView.Calculation(isReset);
					for (int i = 0; i < size; i++)
					{
						string[] value2 = new string[]
						{
							string.Concat(offset + i + 1),
							StringUtil.CheckStringEmpty(personList[i].PersonName),
							StringUtil.CheckStringEmpty(personList[i].PersonSerial),
							StringUtil.CheckStringEmpty(personList[i].Dept),
							personList[i].MonthAttendTotal
						};
						this.recordDataGridView.AddRowData(value2, personList[i].ID.ToString(), null);
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void mySplitListView1_UserControlBtnClicked(object sender, MyEventArgs e)
		{
			try
			{
				this.offset = e.Value;
				this.initData(e.Value, false, false);
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public void initData(int offset, bool isReset, bool isInvoke = false)
		{
			try
			{
				string[] where = new string[5];
				where = (string.IsNullOrEmpty(this.sqlBuilder.ToString()) ? null : this.sqlBuilder.ToString().Split(new char[]
				{
					','
				}));
				this.recordList = this.personInfoService.QueryForList(where, null, null, " id desc ", offset, this.recordDataGridView.PageSize);
				this.addDataGridItem(this.recordList, isReset, offset, isInvoke);
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void select_btn_Click(object sender, EventArgs e)
		{
			try
			{
				bool flag = this.date_time_end.Value.Subtract(this.date_time_start.Value).Days < 0;
				if (flag)
				{
					ErrorDialogForm.ShowErrorMsg("查询结束日期不能早于开始日期!", "", false, null, false);
				}
				else
				{
					int num = (this.date_time_end.Value.Year - this.date_time_start.Value.Year) * 12 + (this.date_time_end.Value.Month - this.date_time_start.Value.Month);
					bool flag2 = num > 5;
					if (flag2)
					{
						ErrorDialogForm.ShowErrorMsg("查询日期跨度不能超过6个月!", "", false, null, false);
					}
					else
					{
						Action expr_C1 = this.ShowLayer;
						if (expr_C1 != null)
						{
							expr_C1();
						}
						ThreadPool.QueueUserWorkItem(new WaitCallback(delegate
						{
							this.queryAndLoadData(0, true);
							base.Invoke(new Action(delegate
							{
								Action expr_07 = this.HideLayer;
								if (expr_07 != null)
								{
									expr_07();
								}
							}));
						}));
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void queryAndLoadData(int startIndex, bool isInvoke = false)
		{
			try
			{
				this.sqlBuilder.Clear();
				string errorMessage = string.Empty;
				string text = this.mytxt_name.Text.Trim();
				this.sqlBuilder.Append(string.Format("((p_valid != -1 and datetime(add_time) <=datetime('{1} 23:59:59')) or (p_valid = -1 and datetime(add_time) <=datetime('{1} 23:59:59') and datetime(update_time) >=datetime('{0} 00:00:00')))", DateTimeUtil.getMonthFirstDay(this.date_time_start.Value).ToString("yyyy-MM-dd"), DateTimeUtil.getMonthLastDay(this.date_time_end.Value).ToString("yyyy-MM-dd")));
				bool flag = !string.IsNullOrWhiteSpace(text);
				if (flag)
				{
					bool flag2 = StringUtil.CommonCheckStringAndNotEmpty(text, "姓名/编号", 30, out errorMessage) > 0;
					if (flag2)
					{
						if (isInvoke)
						{
							base.Invoke(new Action(delegate
							{
								this.Enabled = true;
								ErrorDialogForm.ShowErrorMsg(errorMessage, "", false, null, false);
							}));
						}
						else
						{
							base.Enabled = true;
							ErrorDialogForm.ShowErrorMsg(errorMessage, "", false, null, false);
						}
						return;
					}
					this.sqlBuilder.Append(string.Format(",(person_name like '%{0}%' or person_serial like '%{0}%')", text));
				}
				string[] where = string.IsNullOrEmpty(this.sqlBuilder.ToString()) ? null : this.sqlBuilder.ToString().Split(new char[]
				{
					','
				});
				this.recordList = this.personInfoService.QueryForList(where, null, null, " id desc ", startIndex, this.recordDataGridView.PageSize);
				this.addDataGridItem(this.recordList, true, 0, true);
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void exportBtn_Click(object sender, EventArgs e)
		{
			try
			{
                System.Diagnostics.Process.Start("http://" + WebHttpService.severIPAddress + ":" + WebHttpService.severPort + "/Api?acion=excel&start=" + this.date_time_start.Value.ToString("yyyy-MM-dd") + "&end=" + this.date_time_end.Value.ToString("yyyy-MM-dd"));
                return;

				string[] where = string.IsNullOrEmpty(this.sqlBuilder.ToString()) ? null : this.sqlBuilder.ToString().Split(new char[]
				{
					','
				});
				List<MainInfo> tempExportList = this.personInfoService.QueryForList(where, null, null, " id desc ", -1, -1);
				bool flag = tempExportList == null || tempExportList.Count <= 0;
				if (flag)
				{
					base.Invoke(new Action(delegate
					{
						ErrorDialogForm.ShowErrorMsg("没有任何数据可以导出到Excel文件!", "", false, this, false);
					}));
				}
				else
				{
					bool flag2 = tempExportList != null && tempExportList.Count > 0;
					if (flag2)
					{
						MyFolderBrowserDialog myFolderBrowserDialog = new MyFolderBrowserDialog();
						DialogResult dialogResult = myFolderBrowserDialog.ShowDialog(this);
						bool flag3 = dialogResult == DialogResult.OK;
						if (flag3)
						{
							Action expr_E7 = this.ShowLayer;
							if (expr_E7 != null)
							{
								//expr_E7();
							}
							string dir = myFolderBrowserDialog.DirectoryPath;
							Dictionary<string, List<DateTime>> dateTimeDict = new Dictionary<string, List<DateTime>>();
							Dictionary<string, List<string>> columnsDict = DateTimeUtil.getDayDict(this.search_start, this.search_end, ref dateTimeDict, "MM/dd");
							ThreadPool.QueueUserWorkItem(new WaitCallback(delegate
							{
								string arg = string.Format("考勤记录{0}.xls", DateTime.Now.ToString("yyyyMMdd_HHmmss"));
								List<string> list = new List<string>();
								list.Insert(0, "编号");
								list.Insert(0, "姓名");
								list.Insert(0, "序号");
								Dictionary<int, string> dictionary = new Dictionary<int, string>();
								Dictionary<string, List<MainInfo>> dictionary2 = new Dictionary<string, List<MainInfo>>();
								bool flag4 = dateTimeDict == null || dateTimeDict.Count <= 0;
								if (flag4)
								{
									this.Invoke(new Action(() => this.exportBtn_Click(null,null)));
								}
								foreach (string current in columnsDict.Keys)
								{
									List<MainInfo> list2 = new List<MainInfo>();
									list2.AddRange(tempExportList);
									List<DateTime> list3 = dateTimeDict[current];
									bool flag5 = list3 != null && list3.Count > 0;
									if (flag5)
									{
										dictionary2.Add(current, this.getExportAttendRecord2List(list2, list3[0], list3[list3.Count - 1]));
									}
								}
								bool flag6 = dictionary2 != null && dictionary2.Count > 0;
								if (flag6)
								{
									try
									{
										bool flag7 = ExcelOperate.AttendRecordsToExcel(list, columnsDict, dictionary2, string.Format("{0}/{1}", dir, arg));
										bool flag8 = flag7;
										if (flag8)
										{
											//this.Invoke(new Action(() => this.exportBtn_Click(null,null)));
                                            //MessageBox.Show(arg + "保存成功！");
                                            ErrorDialogForm.ShowErrorMsg(arg + "保存成功！", "", true, this, true);
                                            //UpdateSuccessDialogForm.showMsg(arg + "保存成功！", "", false, null, false);
										}
										else
										{
											this.Invoke(new Action(() => this.exportBtn_Click(null,null)));
										}
									}
									catch (Exception se2)
									{
										LogHelper.LogError(this.GetType(), se2);
										this.Invoke(new Action(() => this.exportBtn_Click(null,null)));
									}
								}
								else
								{
									this.Invoke(new Action(() => this.exportBtn_Click(null,null)));
								}
							}));
						}
					}
					else
					{
						ErrorDialogForm.ShowErrorMsg("查询数据为空,请选择其他查询条件重新导出!", "", false, null, false);
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void getAttendRecord2List(ref List<MainInfo> personInfoList)
		{
			try
			{
				bool flag = personInfoList == null || personInfoList.Count <= 0;
				if (!flag)
				{
					this.search_start = this.date_time_start.Value;
					this.search_end = this.date_time_end.Value;
					string text = string.Empty;
					bool flag2 = personInfoList.Count < 1000;
					if (flag2)
					{
						string text2 = string.Empty;
						StringBuilder personIdBuilder = new StringBuilder();
						personInfoList.ForEach(delegate(MainInfo a)
						{
							personIdBuilder.Append(a.ID + ",");
						});
						bool flag3 = personIdBuilder.Length > 0;
						if (flag3)
						{
							text2 = personIdBuilder.ToString().Substring(0, personIdBuilder.Length - 1);
						}
						text = string.Format(" {2}(datetime(date) >= datetime('{0}') and datetime(date) <= datetime('{1}'))", this.date_time_start.Value.ToString("yyyy-MM-dd"), this.date_time_end.Value.ToString("yyyy-MM-dd"), string.IsNullOrWhiteSpace(text2) ? string.Format("(id in ({0}) and ", text2) : string.Empty);
					}
					else
					{
						text = string.Format(" (datetime(date) >= datetime('{0}') and datetime(date) <= datetime('{1}'))", this.date_time_start.Value.ToString("yyyy-MM-dd"), this.date_time_end.Value.ToString("yyyy-MM-dd"));
					}
					Dictionary<string, AttendanceRecord2> dictionary = this.attendanceRecordService.QueryForAttendRecord(new string[]
					{
						text
					}, null, null, null, -1, -1);
					List<string> dayList = DateTimeUtil.getDayList(this.date_time_start.Value, this.date_time_end.Value, "", "yyyy/MM/dd");
					foreach (MainInfo current in personInfoList)
					{
						List<AttendanceRecord2> list = new List<AttendanceRecord2>();
						int num = 0;
						DateTime now = DateTime.Now;
						DateTime now2 = DateTime.Now;
						bool flag4 = dayList != null && dayList.Count > 0 && DateTime.TryParse(current.AddTime, out now) && DateTime.TryParse(current.UpdateTime, out now2);
						if (flag4)
						{
							foreach (string current2 in dayList)
							{
								AttendanceRecord2 attendanceRecord = new AttendanceRecord2();
								string key = string.Format("{0}${1}", current.ID, current2);
								bool flag5 = dictionary.ContainsKey(key);
								if (flag5)
								{
									attendanceRecord = dictionary[key];
									bool flag6 = 2.Equals(attendanceRecord.AttendStatus) || 1.Equals(attendanceRecord.AttendStatus);
									if (flag6)
									{
										num++;
									}
								}
								else
								{
									attendanceRecord.AttendStatus = 0;
								}
								attendanceRecord.WeekText = DateTimeUtil.Week(DateTime.Parse(current2));
								attendanceRecord.AttendStatusText = "--";
								int attendStatus = attendanceRecord.AttendStatus;
								if (attendStatus != 1)
								{
									if (attendStatus == 2)
									{
										attendanceRecord.AttendStatusText = "正常";
									}
								}
								else
								{
									attendanceRecord.AttendStatusText = "缺勤";
								}
								attendanceRecord.Date = current2;
								bool flag7 = this.isCalculateAttendance(current, now, now2, current2);
								if (flag7)
								{
									list.Add(attendanceRecord);
								}
							}
						}
						current.attendRecordList = list;
						current.MonthAttendTotal = num.ToString();
						bool flag8 = this.recordMap.ContainsKey(current.ID);
						if (flag8)
						{
							this.recordMap[current.ID] = current;
						}
						else
						{
							this.recordMap.Add(current.ID, current);
						}
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private List<MainInfo> getExportAttendRecord2List(List<MainInfo> personInfoList, DateTime startTime, DateTime endTime)
		{
			List<MainInfo> list = new List<MainInfo>();
			List<MainInfo> result;
			try
			{
				bool flag = personInfoList == null || personInfoList.Count <= 0;
				if (flag)
				{
					result = list;
					return result;
				}
				this.search_start = this.date_time_start.Value;
				this.search_end = this.date_time_end.Value;
				string text = string.Empty;
				bool flag2 = personInfoList.Count < 1000;
				if (flag2)
				{
					string text2 = string.Empty;
					StringBuilder personIdBuilder = new StringBuilder();
					personInfoList.ForEach(delegate(MainInfo a)
					{
						personIdBuilder.Append(a.ID + ",");
					});
					bool flag3 = personIdBuilder.Length > 0;
					if (flag3)
					{
						text2 = personIdBuilder.ToString().Substring(0, personIdBuilder.Length - 1);
					}
					text = string.Format(" {2}(datetime(date) >= datetime('{0}') and datetime(date) <= datetime('{1}'))", this.date_time_start.Value.ToString("yyyy-MM-dd"), this.date_time_end.Value.ToString("yyyy-MM-dd"), string.IsNullOrWhiteSpace(text2) ? string.Format("(id in ({0}) and ", text2) : string.Empty);
				}
				else
				{
					text = string.Format(" (datetime(date) >= datetime('{0}') and datetime(date) <= datetime('{1}'))", this.date_time_start.Value.ToString("yyyy-MM-dd"), this.date_time_end.Value.ToString("yyyy-MM-dd"));
				}
				Dictionary<string, AttendanceRecord2> dictionary = this.attendanceRecordService.QueryForAttendRecord(new string[]
				{
					text
				}, null, null, null, -1, -1);
				List<string> dayList = DateTimeUtil.getDayList(startTime, endTime, "", "yyyy/MM/dd");
				foreach (MainInfo current in personInfoList)
				{
					List<AttendanceRecord2> list2 = new List<AttendanceRecord2>();
					int num = 0;
					int num2 = 0;
					int num3 = 0;
					DateTime now = DateTime.Now;
					DateTime now2 = DateTime.Now;
					bool flag4 = dayList != null && dayList.Count > 0 && DateTime.TryParse(current.AddTime, out now) && DateTime.TryParse(current.UpdateTime, out now2);
					if (flag4)
					{
						foreach (string current2 in dayList)
						{
							AttendanceRecord2 attendanceRecord = new AttendanceRecord2();
							string key = string.Format("{0}${1}", current.ID, current2);
							bool flag5 = dictionary.ContainsKey(key);
							if (flag5)
							{
								attendanceRecord = dictionary[key];
							}
							else
							{
								attendanceRecord.AttendStatus = 0;
							}
							attendanceRecord.WeekText = DateTimeUtil.Week(DateTime.Parse(current2));
							attendanceRecord.AttendStatusText = "--";
							int attendStatus = attendanceRecord.AttendStatus;
							if (attendStatus != 1)
							{
								if (attendStatus != 2)
								{
									attendanceRecord.AttendStatusText = "--";
								}
								else
								{
									num3++;
									num++;
									attendanceRecord.AttendStatusText = "正常";
								}
							}
							else
							{
								num3++;
								num2++;
								attendanceRecord.AttendStatusText = "缺勤";
							}
							attendanceRecord.Date = current2;
							bool flag6 = this.isCalculateAttendance(current, now, now2, current2);
							if (flag6)
							{
								list2.Add(attendanceRecord);
							}
						}
					}
					current.MonthAttendTotal = num.ToString();
					current.MonthAbsenceTotal = num2.ToString();
					current.MonthTotal = num3.ToString();
					MainInfo mainInfo = (MainInfo)CommonUtil.CopyOjbect(current);
					mainInfo.attendRecordList = list2;
					bool flag7 = list2 != null && list2.Count > 0;
					if (flag7)
					{
						list.Add(mainInfo);
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
			result = list;
			return result;
		}

		private void recordDataGridView_DataGridViewCheckClicked(object sender, MyEventArgs e)
		{
			try
			{
				DataGridView dataGridView = (DataGridView)sender;
				bool flag = this.dbtEditName.Equals(this.recordDataGridView.Columns[dataGridView.CurrentCell.OwningColumn.Index].Name);
				if (flag)
				{
					string strId = e.StrId;
					bool flag2 = !string.IsNullOrWhiteSpace(strId);
					if (flag2)
					{
						int num = int.Parse(strId);
						bool flag3 = num <= 0;
						if (!flag3)
						{
							MainInfo mainInfo = new MainInfo();
							bool flag4 = this.recordMap.ContainsKey(num);
							if (flag4)
							{
								mainInfo = this.recordMap[num];
							}
							string arg_E1_0 = "{0}~{1}";
							DateTime arg_AB_0 = this.search_start;
							object arg_E1_1 = this.search_start.ToString("yyyy/MM/dd", DateTimeFormatInfo.InvariantInfo);
							DateTime arg_C9_0 = this.search_end;
							string text = string.Format(arg_E1_0, arg_E1_1, this.search_end.ToString("yyyy/MM/dd", DateTimeFormatInfo.InvariantInfo));
							SignRecordDetailForm signRecordDetailForm = new SignRecordDetailForm(mainInfo, text);
							signRecordDetailForm.FormClosed += delegate
							{
								this.baseForm.HideOpaqueLayer();
								this.baseForm.TopMost = true;
							};
							this.baseForm.ShowOpaqueLayer();
							signRecordDetailForm.ShowDialog();
						}
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void mytxt_name_KeyDown(object sender, KeyEventArgs e)
		{
			bool flag = e.KeyCode == Keys.Return;
			if (flag)
			{
				this.select_btn_Click(sender, e);
			}
		}

		private void recordDataGridView_PaginationBtnClicked(object sender, MyEventArgs e)
		{
			this.offset = e.Value;
			Action expr_2C = this.ShowLayer;
			if (expr_2C != null)
			{
				expr_2C();
			}
			ThreadPool.QueueUserWorkItem(new WaitCallback(delegate
			{
				this.initData(e.Value, false, true);
                this.Invoke(new Action(() => 
                    this.recordDataGridView_PaginationBtnClicked(null, null)
                    ));
			}));
		}

		private bool isCalculateAttendance(MainInfo mainInfo, DateTime addDate, DateTime updateDate, string currentDate)
		{
			bool flag = false;
			bool result;
			try
			{
				bool flag2 = !DateTime.TryParse(currentDate, out this.tempDateTime);
				if (flag2)
				{
					result = flag;
					return result;
				}
				DateTime monthFirstDay = DateTimeUtil.getMonthFirstDay(this.tempDateTime);
				DateTime monthLastDay = DateTimeUtil.getMonthLastDay(this.tempDateTime);
				bool flag3 = mainInfo.PValid == -1;
				if (flag3)
				{
					bool flag4 = DateTime.Compare(addDate, monthLastDay) <= 0 && DateTime.Compare(updateDate, monthFirstDay) >= 0;
					if (flag4)
					{
						flag = true;
					}
				}
				else
				{
					bool flag5 = DateTime.Compare(addDate, monthLastDay) <= 0;
					if (flag5)
					{
						result = true;
						return result;
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
			result = flag;
			return result;
		}

		protected override void Dispose(bool disposing)
		{
			bool flag = disposing && this.components != null;
			if (flag)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel = new OpenSoftFace.Setting.Controls.MyTableLayoutPanel();
            this.btn_export = new OpenSoftFace.Setting.Controls.MyButton();
            this.lab_tip = new System.Windows.Forms.Label();
            this.date_time_end = new System.Windows.Forms.DateTimePicker();
            this.date_time_start = new System.Windows.Forms.DateTimePicker();
            this.lbl_name = new System.Windows.Forms.Label();
            this.btn_search = new OpenSoftFace.Setting.Controls.MyButton();
            this.boderPanel = new OpenSoftFace.Setting.Controls.MyBoderPanel();
            this.mytxt_name = new OpenSoftFace.Setting.Controls.MyTextBox();
            this.recordDataGridView = new OpenSoftFace.Setting.Controls.MySplitDataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.boderPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.Location = new System.Drawing.Point(5, 5);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.tableLayoutPanel);
            this.splitContainer.Panel1.Padding = new System.Windows.Forms.Padding(1);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.recordDataGridView);
            this.splitContainer.Panel2.Padding = new System.Windows.Forms.Padding(7, 0, 7, 10);
            this.splitContainer.Size = new System.Drawing.Size(659, 480);
            this.splitContainer.SplitterDistance = 49;
            this.splitContainer.TabIndex = 1;
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel.BorderColor = System.Drawing.Color.Black;
            this.tableLayoutPanel.BorderWidth = 1;
            this.tableLayoutPanel.ColumnCount = 8;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 209F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel.Controls.Add(this.btn_export, 7, 0);
            this.tableLayoutPanel.Controls.Add(this.lab_tip, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.date_time_end, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.date_time_start, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.lbl_name, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.btn_search, 5, 0);
            this.tableLayoutPanel.Controls.Add(this.boderPanel, 4, 0);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(1, 1);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.Padding = new System.Windows.Forms.Padding(10, 2, 10, 3);
            this.tableLayoutPanel.RowCount = 1;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(657, 47);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // btn_export
            // 
            this.btn_export.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_export.EnterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(239)))));
            this.btn_export.EnterForeColor = System.Drawing.Color.White;
            this.btn_export.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(178)))), ((int)(((byte)(178)))));
            this.btn_export.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_export.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_export.ForeColor = System.Drawing.Color.White;
            this.btn_export.LeaveBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_export.LeaveForeColor = System.Drawing.Color.White;
            this.btn_export.Location = new System.Drawing.Point(567, 6);
            this.btn_export.Margin = new System.Windows.Forms.Padding(0, 4, 0, 3);
            this.btn_export.Name = "btn_export";
            this.btn_export.Size = new System.Drawing.Size(80, 31);
            this.btn_export.TabIndex = 5;
            this.btn_export.Text = "导出";
            this.btn_export.UseVisualStyleBackColor = false;
            this.btn_export.Click += new System.EventHandler(this.exportBtn_Click);
            // 
            // lab_tip
            // 
            this.lab_tip.AutoSize = true;
            this.lab_tip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lab_tip.Location = new System.Drawing.Point(138, 2);
            this.lab_tip.Name = "lab_tip";
            this.lab_tip.Size = new System.Drawing.Size(9, 42);
            this.lab_tip.TabIndex = 18;
            this.lab_tip.Text = "-";
            this.lab_tip.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // date_time_end
            // 
            this.date_time_end.CustomFormat = "yyyy/MM/dd";
            this.date_time_end.Dock = System.Windows.Forms.DockStyle.Fill;
            this.date_time_end.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.date_time_end.Location = new System.Drawing.Point(153, 9);
            this.date_time_end.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.date_time_end.Name = "date_time_end";
            this.date_time_end.Size = new System.Drawing.Size(79, 21);
            this.date_time_end.TabIndex = 2;
            // 
            // date_time_start
            // 
            this.date_time_start.CustomFormat = "yyyy/MM/dd";
            this.date_time_start.Dock = System.Windows.Forms.DockStyle.Fill;
            this.date_time_start.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.date_time_start.Location = new System.Drawing.Point(53, 9);
            this.date_time_start.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.date_time_start.MaxDate = new System.DateTime(9998, 12, 24, 0, 0, 0, 0);
            this.date_time_start.Name = "date_time_start";
            this.date_time_start.Size = new System.Drawing.Size(79, 21);
            this.date_time_start.TabIndex = 1;
            // 
            // lbl_name
            // 
            this.lbl_name.AutoSize = true;
            this.lbl_name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_name.Location = new System.Drawing.Point(13, 2);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(34, 42);
            this.lbl_name.TabIndex = 3;
            this.lbl_name.Text = "日期";
            this.lbl_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btn_search
            // 
            this.btn_search.BackColor = System.Drawing.Color.White;
            this.btn_search.EnterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(239)))));
            this.btn_search.EnterForeColor = System.Drawing.Color.White;
            this.btn_search.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_search.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_search.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_search.LeaveBackColor = System.Drawing.Color.White;
            this.btn_search.LeaveForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_search.Location = new System.Drawing.Point(446, 8);
            this.btn_search.Margin = new System.Windows.Forms.Padding(2, 6, 2, 3);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(56, 27);
            this.btn_search.TabIndex = 22;
            this.btn_search.Text = "查询";
            this.btn_search.UseVisualStyleBackColor = false;
            this.btn_search.Click += new System.EventHandler(this.select_btn_Click);
            // 
            // boderPanel
            // 
            this.boderPanel.BackColor = System.Drawing.Color.White;
            this.boderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(178)))), ((int)(((byte)(178)))));
            this.boderPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.boderPanel.BorderWidth = 1;
            this.boderPanel.Controls.Add(this.mytxt_name);
            this.boderPanel.Location = new System.Drawing.Point(238, 9);
            this.boderPanel.Margin = new System.Windows.Forms.Padding(3, 7, 3, 4);
            this.boderPanel.Name = "boderPanel";
            this.boderPanel.Padding = new System.Windows.Forms.Padding(0, 14, 0, 0);
            this.boderPanel.Size = new System.Drawing.Size(203, 29);
            this.boderPanel.TabIndex = 23;
            // 
            // mytxt_name
            // 
            this.mytxt_name.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mytxt_name.BorderColor = System.Drawing.Color.Black;
            this.mytxt_name.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.mytxt_name.EmptyTextTip = "请输入姓名或编号进行搜索";
            this.mytxt_name.EmptyTextTipColor = System.Drawing.Color.DarkGray;
            this.mytxt_name.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.mytxt_name.Location = new System.Drawing.Point(0, 6);
            this.mytxt_name.MaxLength = 20;
            this.mytxt_name.Name = "mytxt_name";
            this.mytxt_name.Size = new System.Drawing.Size(185, 18);
            this.mytxt_name.TabIndex = 2;
            // 
            // recordDataGridView
            // 
            this.recordDataGridView.AllSelectBtnCheckState = false;
            this.recordDataGridView.AllSelectBtnShowState = false;
            this.recordDataGridView.AllSelectBtnText = "全选";
            this.recordDataGridView.AllSize = 0;
            this.recordDataGridView.AutoScroll = true;
            this.recordDataGridView.BackColor = System.Drawing.Color.White;
            this.recordDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recordDataGridView.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.recordDataGridView.IsCheckMouseState = false;
            this.recordDataGridView.Location = new System.Drawing.Point(7, 0);
            this.recordDataGridView.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.recordDataGridView.Name = "recordDataGridView";
            this.recordDataGridView.Offset = 0;
            this.recordDataGridView.PageIndex = 1;
            this.recordDataGridView.PageSize = 5;
            this.recordDataGridView.RefershBtnShowState = false;
            this.recordDataGridView.Size = new System.Drawing.Size(645, 417);
            this.recordDataGridView.TabIndex = 1;
            this.recordDataGridView.TabStop = false;
            this.recordDataGridView.PaginationBtnClicked += new OpenSoftFace.Setting.Controls.MySplitDataGridView.PaginationBtnClickHandle(this.recordDataGridView_PaginationBtnClicked);
            this.recordDataGridView.DataGridViewCheckClicked += new OpenSoftFace.Setting.Controls.MySplitDataGridView.DataGridClickHandle(this.recordDataGridView_DataGridViewCheckClicked);
            // 
            // SignRecordManageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(669, 490);
            this.Controls.Add(this.splitContainer);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SignRecordManageForm";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Text = "SignRecordManageForm";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.SignRecordManageForm_Load);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.boderPanel.ResumeLayout(false);
            this.boderPanel.PerformLayout();
            this.ResumeLayout(false);

		}
	}
}
