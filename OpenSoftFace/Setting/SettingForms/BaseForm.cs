using OpenSoftFace.Setting.Utils;
using OpenSoftFace.Setting.Common;
using OpenSoftFace.Setting.Controls;
using CommonConstant;
using LogUtil;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace OpenSoftFace.Setting.Forms.SettingForms
{
	public class BaseForm : DragBaseForm
	{
		private BasicParamForm basicParamForm;

		private PersonManageForm personManageForm;

		private SignRecordManageForm signRecordManageForm;

		private SignRules signRules;

		private IniHelper iniHelper = new IniHelper("setting.ini", "setting");

		private string rootPath = string.Empty;

		private IContainer components = null;

		private MyVerticalTab myVerticalTab;

		private TabPage tab_basic;

		private TabPage tab_person;

		private TabPage tab_attendance;

		private TabPage tab_attend_setting;

		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams createParams = base.CreateParams;
				createParams.ExStyle |= 33554432;
				return createParams;
			}
		}

		public BaseForm()
		{
			this.InitializeComponent();
			this.InitForms();
			this.initDirPath();
            this.Opacity = 0.96;
		}

		private void initDirPath()
		{
			try
			{
				this.rootPath = AppConfig.GetInstance().ParentDir;
				bool flag = this.iniHelper.Dict.ContainsKey("save_path");
				if (flag)
				{
					string text = this.iniHelper.Dict["save_path"];
					bool flag2 = !string.IsNullOrWhiteSpace(text);
					if (flag2)
					{
						bool flag3 = !Directory.Exists(text);
						if (flag3)
						{
							Directory.CreateDirectory(text);
						}
						this.rootPath = text + "\\";
					}
				}
			}
			catch
			{
			}
		}

		private void InitForms()
		{
			try
			{
				this.basicParamForm = new BasicParamForm(this);
				this.TabAddForm(0, this.basicParamForm);
				this.personManageForm = new PersonManageForm(this);
				this.TabAddForm(1, this.personManageForm);
				this.signRecordManageForm = new SignRecordManageForm(this);
				this.TabAddForm(2, this.signRecordManageForm);
				this.signRules = new SignRules(this);
				this.TabAddForm(3, this.signRules);
				this.signRecordManageForm.ShowLayer += new Action(this.SignRecordManageForm_ShowLayer);
				this.signRecordManageForm.HideLayer += new Action(this.SignRecordManageForm_HideLayer);
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void SignRecordManageForm_HideLayer()
		{
			base.HideMyDragOpaqueLayer();
		}

		private void SignRecordManageForm_ShowLayer()
		{
			base.ShowMyDragOpaqueLayer(this);
		}

		private void TabAddForm(int index, Form form)
		{
			try
			{
				form.TopLevel = false;
				form.Dock = DockStyle.Fill;
				form.FormBorderStyle = FormBorderStyle.None;
				form.Show();
				this.myVerticalTab.TabPages[index].Controls.Add(form);
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void BaseForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			try
			{
				OperatorUtil.DeleteTempFile(this.rootPath);
				OperatorUtil.DeleteInvalidShowFile(this.rootPath);
				SDKOperate.UninitEngine();
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void myVerticalTab_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				bool flag = this.myVerticalTab.TabPages != null && this.myVerticalTab.TabPages.Count > 3;
				if (flag)
				{
					bool flag2 = this.myVerticalTab.SelectedIndex == 2;
					if (flag2)
					{
						this.signRecordManageForm.Reload();
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void btn_close_Click(object sender, EventArgs e)
		{
			try
			{
                //this.Close();
                //this.Hide();
				Environment.Exit(0);
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		protected override void Dispose(bool disposing)
		{
			bool flag = disposing && this.components != null;
			if (flag)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaseForm));
            this.myVerticalTab = new OpenSoftFace.Setting.Controls.MyVerticalTab();
            this.tab_basic = new System.Windows.Forms.TabPage();
            this.tab_person = new System.Windows.Forms.TabPage();
            this.tab_attendance = new System.Windows.Forms.TabPage();
            this.tab_attend_setting = new System.Windows.Forms.TabPage();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_hide)).BeginInit();
            this.myVerticalTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(248)))));
            this.splitContainer.IsSplitterFixed = true;
            this.splitContainer.Margin = new System.Windows.Forms.Padding(0);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.myVerticalTab);
            this.splitContainer.Size = new System.Drawing.Size(900, 720);
            this.splitContainer.SplitterDistance = 68;
            this.splitContainer.SplitterWidth = 1;
            // 
            // btn_close
            // 
            this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(72)))), ((int)(((byte)(109)))));
            this.btn_close.Image = ((System.Drawing.Image)(resources.GetObject("btn_close.Image")));
            this.btn_close.Location = new System.Drawing.Point(876, 28);
            this.btn_close.Size = new System.Drawing.Size(16, 16);
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // lbl_title
            // 
            this.lbl_title.Font = new System.Drawing.Font("微软雅黑", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_title.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_title.Size = new System.Drawing.Size(900, 68);
            this.lbl_title.Text = "AI人脸考勤";
            // 
            // btn_hide
            // 
            this.btn_hide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(72)))), ((int)(((byte)(109)))));
            this.btn_hide.Location = new System.Drawing.Point(853, 28);
            this.btn_hide.Size = new System.Drawing.Size(16, 16);
            this.btn_hide.Click += new System.EventHandler(this.btn_hide_Click);
            // 
            // myVerticalTab
            // 
            this.myVerticalTab.Controls.Add(this.tab_basic);
            this.myVerticalTab.Controls.Add(this.tab_person);
            this.myVerticalTab.Controls.Add(this.tab_attendance);
            this.myVerticalTab.Controls.Add(this.tab_attend_setting);
            this.myVerticalTab.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.myVerticalTab.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.myVerticalTab.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.myVerticalTab.ItemSize = new System.Drawing.Size(120, 50);
            this.myVerticalTab.LeftTabIndex = -3;
            this.myVerticalTab.Location = new System.Drawing.Point(0, 0);
            this.myVerticalTab.Margin = new System.Windows.Forms.Padding(0);
            this.myVerticalTab.Multiline = true;
            this.myVerticalTab.Name = "myVerticalTab";
            this.myVerticalTab.Padding = new System.Drawing.Point(0, 0);
            this.myVerticalTab.SelectedIndex = 0;
            this.myVerticalTab.Size = new System.Drawing.Size(900, 651);
            this.myVerticalTab.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.myVerticalTab.TabIndex = 1;
            this.myVerticalTab.TabStop = false;
            this.myVerticalTab.SelectedIndexChanged += new System.EventHandler(this.myVerticalTab_SelectedIndexChanged);
            // 
            // tab_basic
            // 
            this.tab_basic.BackColor = System.Drawing.Color.White;
            this.tab_basic.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tab_basic.Location = new System.Drawing.Point(0, 50);
            this.tab_basic.Margin = new System.Windows.Forms.Padding(0);
            this.tab_basic.Name = "tab_basic";
            this.tab_basic.Size = new System.Drawing.Size(900, 601);
            this.tab_basic.TabIndex = 0;
            this.tab_basic.Text = "基础设置";
            // 
            // tab_person
            // 
            this.tab_person.BackColor = System.Drawing.Color.White;
            this.tab_person.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tab_person.Location = new System.Drawing.Point(0, 50);
            this.tab_person.Margin = new System.Windows.Forms.Padding(0);
            this.tab_person.Name = "tab_person";
            this.tab_person.Size = new System.Drawing.Size(900, 601);
            this.tab_person.TabIndex = 2;
            this.tab_person.Text = "人员管理";
            // 
            // tab_attendance
            // 
            this.tab_attendance.BackColor = System.Drawing.Color.White;
            this.tab_attendance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tab_attendance.Location = new System.Drawing.Point(0, 50);
            this.tab_attendance.Margin = new System.Windows.Forms.Padding(0);
            this.tab_attendance.Name = "tab_attendance";
            this.tab_attendance.Size = new System.Drawing.Size(900, 601);
            this.tab_attendance.TabIndex = 3;
            this.tab_attendance.Text = "考勤管理";
            // 
            // tab_attend_setting
            // 
            this.tab_attend_setting.BackColor = System.Drawing.Color.White;
            this.tab_attend_setting.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tab_attend_setting.Location = new System.Drawing.Point(0, 50);
            this.tab_attend_setting.Margin = new System.Windows.Forms.Padding(0);
            this.tab_attend_setting.Name = "tab_attend_setting";
            this.tab_attend_setting.Size = new System.Drawing.Size(900, 601);
            this.tab_attend_setting.TabIndex = 4;
            this.tab_attend_setting.Text = "考勤规则";
            // 
            // BaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 720);
            this.Name = "BaseForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AI人脸考勤";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BaseForm_FormClosing);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_hide)).EndInit();
            this.myVerticalTab.ResumeLayout(false);
            this.ResumeLayout(false);

		}

        private void btn_hide_Click(object sender, EventArgs e)
        {

        }
	}
}
