using OpenSoftFace.Setting.Common;
using OpenSoftFace.Setting.Controls;
using CommonConstant;
using LogUtil;
using Model.Common;
using Model.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Utils;

namespace OpenSoftFace.Setting.Forms.SettingForms
{
	public class SignRecordDetailForm : DragBaseForm
	{
		private MainInfo mainInfo;

		private string attendDateStr;

        private string attendDir = System.Environment.CurrentDirectory + "\\OpenSoftFaceFile\\check\\";

		private IniHelper iniHelper = new IniHelper("setting.ini", "setting");

		private IContainer components = null;

		private SplitContainer container_main;

		private TableLayoutPanel tabPanel_image;

		private PictureBox pic_start;

		private PictureBox pic_end;

		private Label lbl_start;

        private Label lbl_end;

		private Label lbl_date;

		private Label lbl_num;

		private Label lbl_name;

		private Label lbl_dept;

        private SplitContainer splitContainer1;

		private MySplitDataGridView recordDataGridView;

		public SignRecordDetailForm(string id)
		{
			this.InitializeComponent();
            this.Opacity = 0.96;
		}

		public SignRecordDetailForm(MainInfo mainInfo, string text)
		{
			this.InitializeComponent();
			this.mainInfo = mainInfo;
			this.attendDateStr = text;
            this.Opacity = 0.96;
		}

		private void SignRecordDetailForm_Load(object sender, EventArgs e)
		{
			try
			{
				base.TopMost = true;
				this.initDirPath();
				Dictionary<string, int> dictValue = new Dictionary<string, int>
				{
					{
						"序号",
						60
					},
					{
						"日期",
						100
					},
					{
						"上班时间",
						0
					},
					{
						"下班时间",
						0
					},
					{
						"考勤状态",
						0
					}
				};
				this.recordDataGridView.CreateListHeader(dictValue, 0);
				this.recordDataGridView.HidePagingButton();
				this.initLabel();
				this.recordDataGridView.FirstRowClicked();
				this.recordDataGridView.HideCheckBox();
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void initDirPath()
		{
			try
			{
                string str = Directory.GetParent(Application.StartupPath).FullName + "\\OpenSoftFaceFile\\";
				bool flag = this.iniHelper.Dict.ContainsKey("save_path");
				if (flag)
				{
					string text = this.iniHelper.Dict["save_path"];
					bool flag2 = !string.IsNullOrWhiteSpace(text);
					if (flag2)
					{
						bool flag3 = !Directory.Exists(text);
						if (flag3)
						{
							Directory.CreateDirectory(text);
						}
						str = text + "\\";
					}
				}
				this.attendDir = str + "check\\";
			}
			catch
			{
			}
		}

		private void initLabel()
		{
			try
			{
				bool flag = this.mainInfo != null;
				if (flag)
				{
					this.lbl_date.Text = string.Format("考勤范围：{0}", this.attendDateStr);
					this.lbl_name.Text = "姓名：" + StringUtil.CheckStringEmpty(this.mainInfo.PersonName);
					this.lbl_num.Text = "编号：" + StringUtil.CheckStringEmpty(this.mainInfo.PersonSerial);
					this.lbl_dept.Text = "部门：" + StringUtil.CheckStringEmpty(this.mainInfo.Dept);
					this.addDataGridItem(this.mainInfo.attendRecordList);
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public void addDataGridItem(List<AttendanceRecord2> recordList)
		{
			try
			{
				bool flag = recordList == null || recordList.Count <= 0;
				if (!flag)
				{
					this.recordDataGridView.ClearDataGridView();
					int count = recordList.Count;
					this.recordDataGridView.AllSize = count;
					this.recordDataGridView.Calculation(false);
					for (int i = 0; i < count; i++)
					{
						string[] value = new string[]
						{
							(i + 1).ToString(),
							StringUtil.CheckStringEmpty(recordList[i].Date),
							StringUtil.CheckStringEmpty(recordList[i].StartTime),
							StringUtil.CheckStringEmpty(recordList[i].EndTime),
							StringUtil.CheckStringEmpty(recordList[i].AttendStatusText)
						};
						string text = this.attendDir + recordList[i].StartAttendUrl;
						text = text.Replace("/", "\\").Replace("\\\\", "\\").Replace("\\\\\\", "\\");
						string text2 = this.attendDir + recordList[i].EndAttendUrl;
						text2 = text2.Replace("/", "\\").Replace("\\\\", "\\").Replace("\\\\\\", "\\");
						this.recordDataGridView.AddRowData(value, text + "$" + text2, null);
					}
					this.recordDataGridView.Calculation(false);
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void mySplitDataGridView1_dataGridViewCheckClicked(object sender, MyEventArgs e)
		{
			try
			{
				string strId = e.StrId;
				bool flag = !string.IsNullOrEmpty(strId);
				if (flag)
				{
					string[] array = strId.Split(new char[]
					{
						'$'
					});
					bool flag2 = !string.IsNullOrWhiteSpace(array[0]);
					if (flag2)
					{
						this.pic_start.ImageLocation = array[0];
					}
					else
					{
						//this.pic_start.Image = Resources.noPerson;
					}
					bool flag3 = !string.IsNullOrWhiteSpace(array[1]);
					if (flag3)
					{
						this.pic_end.ImageLocation = array[1];
					}
					else
					{
						//this.pic_end.Image = Resources.noPerson;
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		protected override void Dispose(bool disposing)
		{
			bool flag = disposing && this.components != null;
			if (flag)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SignRecordDetailForm));
            this.container_main = new System.Windows.Forms.SplitContainer();
            this.recordDataGridView = new OpenSoftFace.Setting.Controls.MySplitDataGridView();
            this.tabPanel_image = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_dept = new System.Windows.Forms.Label();
            this.lbl_num = new System.Windows.Forms.Label();
            this.pic_start = new System.Windows.Forms.PictureBox();
            this.lbl_name = new System.Windows.Forms.Label();
            this.pic_end = new System.Windows.Forms.PictureBox();
            this.lbl_start = new System.Windows.Forms.Label();
            this.lbl_date = new System.Windows.Forms.Label();
            this.lbl_end = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_hide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.container_main)).BeginInit();
            this.container_main.Panel1.SuspendLayout();
            this.container_main.Panel2.SuspendLayout();
            this.container_main.SuspendLayout();
            this.tabPanel_image.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_start)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_end)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.IsSplitterFixed = true;
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.splitContainer1);
            this.splitContainer.Size = new System.Drawing.Size(850, 703);
            this.splitContainer.SplitterDistance = 55;
            this.splitContainer.TabStop = false;
            // 
            // btn_close
            // 
            this.btn_close.Image = ((System.Drawing.Image)(resources.GetObject("btn_close.Image")));
            this.btn_close.Location = new System.Drawing.Point(829, 10);
            this.btn_close.Size = new System.Drawing.Size(16, 16);
            // 
            // lbl_title
            // 
            this.lbl_title.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_title.Size = new System.Drawing.Size(850, 55);
            this.lbl_title.Text = "  考勤详情";
            // 
            // container_main
            // 
            this.container_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.container_main.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.container_main.IsSplitterFixed = true;
            this.container_main.Location = new System.Drawing.Point(0, 0);
            this.container_main.Name = "container_main";
            // 
            // container_main.Panel1
            // 
            this.container_main.Panel1.Controls.Add(this.recordDataGridView);
            this.container_main.Panel1.Padding = new System.Windows.Forms.Padding(10, 5, 10, 5);
            // 
            // container_main.Panel2
            // 
            this.container_main.Panel2.Controls.Add(this.tabPanel_image);
            this.container_main.Panel2.Padding = new System.Windows.Forms.Padding(0, 20, 0, 0);
            this.container_main.Size = new System.Drawing.Size(850, 641);
            this.container_main.SplitterDistance = 650;
            this.container_main.TabIndex = 0;
            this.container_main.TabStop = false;
            // 
            // recordDataGridView
            // 
            this.recordDataGridView.AllSelectBtnCheckState = false;
            this.recordDataGridView.AllSelectBtnShowState = false;
            this.recordDataGridView.AllSelectBtnText = "全选";
            this.recordDataGridView.AllSize = 0;
            this.recordDataGridView.AutoScroll = true;
            this.recordDataGridView.BackColor = System.Drawing.Color.White;
            this.recordDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recordDataGridView.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.recordDataGridView.IsCheckMouseState = false;
            this.recordDataGridView.Location = new System.Drawing.Point(10, 5);
            this.recordDataGridView.Margin = new System.Windows.Forms.Padding(0);
            this.recordDataGridView.Name = "recordDataGridView";
            this.recordDataGridView.Offset = 0;
            this.recordDataGridView.PageIndex = 1;
            this.recordDataGridView.PageSize = 10;
            this.recordDataGridView.RefershBtnShowState = false;
            this.recordDataGridView.Size = new System.Drawing.Size(630, 631);
            this.recordDataGridView.TabIndex = 1;
            this.recordDataGridView.DataGridViewCheckClicked += new OpenSoftFace.Setting.Controls.MySplitDataGridView.DataGridClickHandle(this.mySplitDataGridView1_dataGridViewCheckClicked);
            // 
            // tabPanel_image
            // 
            this.tabPanel_image.ColumnCount = 1;
            this.tabPanel_image.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tabPanel_image.Controls.Add(this.lbl_dept, 0, 7);
            this.tabPanel_image.Controls.Add(this.lbl_num, 0, 6);
            this.tabPanel_image.Controls.Add(this.pic_start, 0, 1);
            this.tabPanel_image.Controls.Add(this.lbl_name, 0, 5);
            this.tabPanel_image.Controls.Add(this.pic_end, 0, 3);
            this.tabPanel_image.Controls.Add(this.lbl_start, 0, 0);
            this.tabPanel_image.Controls.Add(this.lbl_date, 0, 4);
            this.tabPanel_image.Controls.Add(this.lbl_end, 0, 2);
            this.tabPanel_image.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPanel_image.Location = new System.Drawing.Point(0, 20);
            this.tabPanel_image.Name = "tabPanel_image";
            this.tabPanel_image.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.tabPanel_image.RowCount = 8;
            this.tabPanel_image.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tabPanel_image.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tabPanel_image.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tabPanel_image.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tabPanel_image.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tabPanel_image.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tabPanel_image.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tabPanel_image.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tabPanel_image.Size = new System.Drawing.Size(196, 621);
            this.tabPanel_image.TabIndex = 0;
            // 
            // lbl_dept
            // 
            this.lbl_dept.AutoEllipsis = true;
            this.lbl_dept.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_dept.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.lbl_dept.Location = new System.Drawing.Point(0, 568);
            this.lbl_dept.Margin = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lbl_dept.Name = "lbl_dept";
            this.lbl_dept.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.lbl_dept.Size = new System.Drawing.Size(186, 43);
            this.lbl_dept.TabIndex = 3;
            this.lbl_dept.Text = "部门";
            this.lbl_dept.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_num
            // 
            this.lbl_num.AutoEllipsis = true;
            this.lbl_num.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_num.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.lbl_num.Location = new System.Drawing.Point(0, 506);
            this.lbl_num.Margin = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lbl_num.Name = "lbl_num";
            this.lbl_num.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.lbl_num.Size = new System.Drawing.Size(186, 42);
            this.lbl_num.TabIndex = 2;
            this.lbl_num.Text = "编号";
            this.lbl_num.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pic_start
            // 
            this.pic_start.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pic_start.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_start.Location = new System.Drawing.Point(6, 38);
            this.pic_start.Name = "pic_start";
            this.pic_start.Size = new System.Drawing.Size(173, 140);
            this.pic_start.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_start.TabIndex = 0;
            this.pic_start.TabStop = false;
            // 
            // lbl_name
            // 
            this.lbl_name.AutoEllipsis = true;
            this.lbl_name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_name.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.lbl_name.Location = new System.Drawing.Point(0, 444);
            this.lbl_name.Margin = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.lbl_name.Size = new System.Drawing.Size(186, 42);
            this.lbl_name.TabIndex = 1;
            this.lbl_name.Text = "姓名";
            this.lbl_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pic_end
            // 
            this.pic_end.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pic_end.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_end.Location = new System.Drawing.Point(6, 224);
            this.pic_end.Name = "pic_end";
            this.pic_end.Size = new System.Drawing.Size(173, 140);
            this.pic_end.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_end.TabIndex = 1;
            this.pic_end.TabStop = false;
            // 
            // lbl_start
            // 
            this.lbl_start.AutoSize = true;
            this.lbl_start.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_start.Location = new System.Drawing.Point(3, 0);
            this.lbl_start.Name = "lbl_start";
            this.lbl_start.Size = new System.Drawing.Size(180, 31);
            this.lbl_start.TabIndex = 2;
            this.lbl_start.Text = "上班打卡图片";
            this.lbl_start.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbl_date
            // 
            this.lbl_date.AutoEllipsis = true;
            this.lbl_date.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_date.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.lbl_date.Location = new System.Drawing.Point(0, 382);
            this.lbl_date.Margin = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lbl_date.Name = "lbl_date";
            this.lbl_date.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.lbl_date.Size = new System.Drawing.Size(186, 42);
            this.lbl_date.TabIndex = 0;
            this.lbl_date.Text = "日期";
            this.lbl_date.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_end
            // 
            this.lbl_end.AutoSize = true;
            this.lbl_end.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_end.Location = new System.Drawing.Point(3, 186);
            this.lbl_end.Name = "lbl_end";
            this.lbl_end.Size = new System.Drawing.Size(180, 31);
            this.lbl_end.TabIndex = 3;
            this.lbl_end.Text = "下班打卡图片";
            this.lbl_end.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.splitContainer1.Panel1MinSize = 2;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.container_main);
            this.splitContainer1.Size = new System.Drawing.Size(850, 644);
            this.splitContainer1.SplitterDistance = 2;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 1;
            this.splitContainer1.TabStop = false;
            // 
            // SignRecordDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(850, 703);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "SignRecordDetailForm";
            this.ShowInTaskbar = false;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.SignRecordDetailForm_Load);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_hide)).EndInit();
            this.container_main.Panel1.ResumeLayout(false);
            this.container_main.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.container_main)).EndInit();
            this.container_main.ResumeLayout(false);
            this.tabPanel_image.ResumeLayout(false);
            this.tabPanel_image.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_start)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_end)).EndInit();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

		}
	}
}
