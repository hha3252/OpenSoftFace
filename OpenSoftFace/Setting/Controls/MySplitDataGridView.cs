using OpenSoftFace.Setting.Common;
using CCWin.SkinControl;
using CommonConstant;
using LogUtil;
using Model.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace OpenSoftFace.Setting.Controls
{
	public class MySplitDataGridView : UserControl
	{
		public delegate void PaginationBtnClickHandle(object sender, MyEventArgs e);

		public delegate void DataGridClickHandle(object sender, MyEventArgs e);

		private int m_allSize = 0;

		private int m_pageIndex = 1;

		private int m_offset = 0;

		private int m_pageSize = 10;

		private int buttonIndex = 0;

		private bool _IsShowButton = false;

		private bool _isAllSelectBtn = false;

		private bool _isShowAllSelectBtn = false;

		private bool _isShowRefershBtn = false;

		private string _allSelectBtnText = string.Empty;

		private bool _isCheckMouseState = false;

		private Image tempImage = null;

		private int gifFrameIndex = 0;

		private IContainer components = null;

		private TableLayoutPanel tableLayoutPanel;

		private SkinLabel lbl_index;

		private MyButton btn_prev;

		private MyButton btn_next;

		private MyButton btn_end;

		private MyButton btn_first;

		private SkinLabel lbl_count;

		private SkinDataGridView skinDataGridView;

		private MyButton btn_allSelect;

		private Timer timer_gif;

		private MyButton btn_refersh;

		[method: CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
		public event MySplitDataGridView.PaginationBtnClickHandle PaginationBtnClicked;

		[method: CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
		public event MySplitDataGridView.DataGridClickHandle DataGridViewCheckClicked;

		[method: CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
		public event Action<int> onScrollToEnd;

		[method: CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
		public event Action RefershClick = null;

		public bool AllSelectBtnCheckState
		{
			get
			{
				return this._isAllSelectBtn;
			}
			set
			{
				this._isAllSelectBtn = value;
			}
		}

		public int Offset
		{
			get
			{
				return this.m_offset;
			}
			set
			{
				this.m_offset = value;
			}
		}

		public string AllSelectBtnText
		{
			get
			{
				return this.btn_allSelect.Text;
			}
			set
			{
				bool flag = !string.IsNullOrWhiteSpace(value);
				if (flag)
				{
					this._allSelectBtnText = value;
					this.btn_allSelect.Text = value;
				}
			}
		}

		public bool AllSelectBtnShowState
		{
			get
			{
				return this._isShowAllSelectBtn;
			}
			set
			{
				this._isShowAllSelectBtn = value;
				this.btn_allSelect.Visible = value;
			}
		}

		public bool RefershBtnShowState
		{
			get
			{
				return this._isShowRefershBtn;
			}
			set
			{
				this._isShowRefershBtn = value;
				this.btn_refersh.Visible = value;
			}
		}

		public bool IsCheckMouseState
		{
			get
			{
				return this._isCheckMouseState;
			}
			set
			{
				this._isCheckMouseState = value;
			}
		}

		public int AllSize
		{
			get
			{
				return this.m_allSize;
			}
			set
			{
				this.m_allSize = value;
			}
		}

		public int PageSize
		{
			get
			{
				return this.m_pageSize;
			}
			set
			{
				this.m_pageSize = value;
			}
		}

		public int PageIndex
		{
			get
			{
				return this.m_pageIndex;
			}
			set
			{
				this.m_pageIndex = value;
			}
		}

		public DataGridViewColumnCollection Columns
		{
			get
			{
				return this.skinDataGridView.Columns;
			}
		}

		public int HeaderHeight
		{
			set
			{
				this.skinDataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
				this.skinDataGridView.ColumnHeadersHeight = value;
				this.skinDataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			}
		}

		public int RowHeight
		{
			set
			{
				this.skinDataGridView.RowTemplate.Height = value;
			}
		}

		public int RowCounts()
		{
			return this.skinDataGridView.Rows.Count;
		}

		public int CurrentRow()
		{
			return this.skinDataGridView.CurrentRow.Index;
		}

		public MySplitDataGridView()
		{
			this.InitializeComponent();
			this.skinDataGridView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
			this.skinDataGridView.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
			this.skinDataGridView.AlternatingRowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			this.skinDataGridView.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			this.skinDataGridView.RowHeadersVisible = false;
			this.skinDataGridView.AutoGenerateColumns = false;
			this.skinDataGridView.AllowUserToAddRows = false;
			this.skinDataGridView.AllowUserToResizeRows = false;
			this.skinDataGridView.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
			this.skinDataGridView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
			this.skinDataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
			this.skinDataGridView.ColumnHeadersHeight = 50;
			this.skinDataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.skinDataGridView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
			this.skinDataGridView.RowTemplate.Height = 40;
			this.skinDataGridView.ScrollBars = ScrollBars.Vertical;
			this.skinDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
			this.skinDataGridView.TitleBackColorBegin = ColorUtil.LightSlateGray;
			this.skinDataGridView.TitleBackColorEnd = ColorUtil.LightSlateGray;
			this.skinDataGridView.AlternatingCellBackColor = ColorUtil.LightSlateGray;
			this.skinDataGridView.MouseCellBackColor = ColorUtil.LightSkyBlue;
			this.skinDataGridView.RowsDefaultCellStyle.SelectionBackColor = ColorUtil.LightSkyBlue;
			this.skinDataGridView.RowsDefaultCellStyle.SelectionForeColor = ColorUtil.Black;
			this.skinDataGridView.ColumnSelectBackColor = ColorUtil.LightSkyBlue;
			this.skinDataGridView.ColumnSelectForeColor = ColorUtil.Black;
			this.skinDataGridView.AlternatingRowsDefaultCellStyle.SelectionBackColor = ColorUtil.LightSkyBlue;
			this.skinDataGridView.AlternatingRowsDefaultCellStyle.SelectionForeColor = ColorUtil.Black;
			this.skinDataGridView.CellBorderStyle = DataGridViewCellBorderStyle.None;
			this.btn_allSelect.Visible = this._isShowAllSelectBtn;
		}

		public void CreateListHeader(string[] value, int checkBoxWidth = 0)
		{
			try
			{
				this.AddCheckBox(checkBoxWidth);
				for (int i = 0; i < value.Count<string>(); i++)
				{
					string[] array = value[i].Split(new char[]
					{
						','
					});
					string headerText = array[0];
					string dataPropertyName = (array.Count<string>() == 1) ? "" : array[1];
					this.skinDataGridView.Columns.Add(new DataGridViewTextBoxColumn
					{
						HeaderText = headerText,
						DataPropertyName = dataPropertyName
					});
				}
				for (int j = 0; j < this.skinDataGridView.Columns.Count - 1; j++)
				{
					this.skinDataGridView.Columns[j + 1].ReadOnly = true;
					this.skinDataGridView.Columns[j + 1].SortMode = DataGridViewColumnSortMode.NotSortable;
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public void CreateListHeader(Dictionary<string, int> dictValue, int checkBoxWidth = 0)
		{
			try
			{
				this.AddCheckBox(checkBoxWidth);
				foreach (string current in dictValue.Keys)
				{
					string[] array = current.Split(new char[]
					{
						','
					});
					string headerText = array[0];
					string dataPropertyName = (array.Count<string>() == 1) ? string.Empty : array[1];
					bool flag = string.IsNullOrWhiteSpace(current);
					if (flag)
					{
						this.skinDataGridView.Columns.Add(new DataGridViewTextBoxColumn
						{
							HeaderText = headerText,
							DataPropertyName = dataPropertyName,
							AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
						});
					}
					else
					{
						bool flag2 = dictValue[current] == 0;
						if (flag2)
						{
							this.skinDataGridView.Columns.Add(new DataGridViewTextBoxColumn
							{
								HeaderText = headerText,
								DataPropertyName = dataPropertyName,
								AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill,
								MinimumWidth = 50
							});
						}
						else
						{
							this.skinDataGridView.Columns.Add(new DataGridViewTextBoxColumn
							{
								HeaderText = headerText,
								DataPropertyName = dataPropertyName,
								AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill,
								MinimumWidth = dictValue[current]
							});
						}
					}
				}
				for (int i = 0; i < this.skinDataGridView.Columns.Count - 1; i++)
				{
					this.skinDataGridView.Columns[i + 1].ReadOnly = true;
					this.skinDataGridView.Columns[i + 1].SortMode = DataGridViewColumnSortMode.NotSortable;
				}
				this.skinDataGridView.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public void AddDataSource(object list_value, Dictionary<int, string> propertyNameMap)
		{
			try
			{
				bool flag = propertyNameMap != null && propertyNameMap.Count > 0;
				if (flag)
				{
					foreach (KeyValuePair<int, string> current in propertyNameMap)
					{
						bool flag2 = this.skinDataGridView.Columns.Count > current.Key;
						if (flag2)
						{
							this.skinDataGridView.Columns[current.Key].DataPropertyName = current.Value;
						}
					}
				}
				this.skinDataGridView.DataSource = list_value;
				bool flag3 = this.skinDataGridView.Columns.Count > 0 && this.skinDataGridView.Columns[0] is CheckBoxTextColumn && this.skinDataGridView.Rows.Count > 0;
				if (flag3)
				{
					for (int i = 0; i < this.skinDataGridView.Rows.Count; i++)
					{
						((CheckBoxTextCell)this.skinDataGridView.Rows[i].Cells[0]).Text = (i + 1).ToString();
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public void AddRowData(string[] value, string tag = null, string serialValue = null)
		{
			try
			{
				bool flag = this.skinDataGridView.Columns != null && this.skinDataGridView.Columns.Count > 0;
				if (flag)
				{
					int index = this.skinDataGridView.Rows.Add();
					this.skinDataGridView.Rows[index].Tag = tag;
					for (int i = 0; i < value.Count<string>(); i++)
					{
						this.skinDataGridView.Rows[index].Cells[i + 1].Value = value[i];
					}
					bool flag2 = !string.IsNullOrWhiteSpace(serialValue) && this.skinDataGridView.Rows[index].Cells[0] is CheckBoxTextCell;
					if (flag2)
					{
						((CheckBoxTextCell)this.skinDataGridView.Rows[index].Cells[0]).Text = serialValue;
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public void Header_OnCheckBoxClicked(bool state)
		{
			try
			{
				this.skinDataGridView.EndEdit();
				this.skinDataGridView.Rows.OfType<DataGridViewRow>().ToList<DataGridViewRow>().ForEach(delegate(DataGridViewRow t)
				{
					t.Cells[0].Value = state;
				});
				bool flag = !state;
				if (flag)
				{
					bool allSelectBtnShowState = this.AllSelectBtnShowState;
					if (allSelectBtnShowState)
					{
						this.AllSelectBtnText = "全选";
						this.AllSelectBtnCheckState = false;
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public void AddCheckBox(int checkBoxWidth = 0)
		{
			try
			{
				CheckBoxTextColumn checkBoxTextColumn = new CheckBoxTextColumn();
				checkBoxTextColumn.Name = "*";
				DatagridViewCheckBoxHeaderCell datagridViewCheckBoxHeaderCell = new DatagridViewCheckBoxHeaderCell(false);
				checkBoxTextColumn.HeaderCell = datagridViewCheckBoxHeaderCell;
				datagridViewCheckBoxHeaderCell.OnCheckBoxClicked += new CheckBoxClickedHandler(this.Header_OnCheckBoxClicked);
				bool flag = checkBoxWidth != 0;
				if (flag)
				{
					checkBoxTextColumn.Width = checkBoxWidth;
				}
				else
				{
					checkBoxTextColumn.Width = 80;
				}
				checkBoxTextColumn.HeaderText = "";
				checkBoxTextColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
				this.skinDataGridView.Columns.Insert(0, checkBoxTextColumn);
				this.skinDataGridView.Columns[0].ReadOnly = false;
				this.skinDataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public void CheckBoxSelect(bool isCheck)
		{
			try
			{
				this.skinDataGridView.Rows.OfType<DataGridViewRow>().ToList<DataGridViewRow>().ForEach(delegate(DataGridViewRow t)
				{
					t.Cells[0].Value = isCheck;
				});
				this.CancelSelectedForHeader(isCheck);
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public void CancelSelectedForHeader(bool isCheck)
		{
			try
			{
				DatagridViewCheckBoxHeaderCell datagridViewCheckBoxHeaderCell = (DatagridViewCheckBoxHeaderCell)this.skinDataGridView.Columns[0].HeaderCell;
				datagridViewCheckBoxHeaderCell.SetCheck(isCheck);
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public void AddButton(DataGridViewTextBoxColumn colBtn, DataGridViewAutoSizeColumnMode autoSizeMode = DataGridViewAutoSizeColumnMode.None)
		{
			try
			{
				colBtn.AutoSizeMode = autoSizeMode;
				colBtn.CellTemplate.Style.ForeColor = ColorUtil.DeepBlue;
				colBtn.CellTemplate.Style.SelectionForeColor = ColorUtil.DeepBlue;
				colBtn.CellTemplate.Style.SelectionBackColor = ColorUtil.LightSkyBlue;
				colBtn.DefaultCellStyle.ForeColor = ColorUtil.DeepBlue;
				colBtn.DefaultCellStyle.SelectionForeColor = ColorUtil.DeepBlue;
				colBtn.DefaultCellStyle.SelectionBackColor = ColorUtil.LightSkyBlue;
				colBtn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
				colBtn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
				colBtn.SortMode = DataGridViewColumnSortMode.NotSortable;
				colBtn.ReadOnly = true;
				int num = this.skinDataGridView.Columns.Add(colBtn);
				this.skinDataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
				this._IsShowButton = true;
				this.buttonIndex = num;
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public void HideCheckBox()
		{
			this.skinDataGridView.Columns[0].Visible = false;
		}

		public void HidePagingButton()
		{
			this.tableLayoutPanel.Visible = false;
		}

		public void HideLabel()
		{
			this.lbl_count.Visible = false;
		}

		public List<int> GetCheckedItemIndex()
		{
			List<int> list = new List<int>();
			bool flag = this.skinDataGridView.Rows.Count > 0;
			if (flag)
			{
				try
				{
					for (int i = 0; i < this.skinDataGridView.Rows.Count; i++)
					{
						CheckBoxTextCell checkBoxTextCell = (CheckBoxTextCell)this.skinDataGridView.Rows[i].Cells[0];
						bool flag2 = Convert.ToBoolean(checkBoxTextCell.Value);
						bool flag3 = flag2;
						if (flag3)
						{
							list.Add(this.skinDataGridView.Rows[i].Index);
						}
					}
				}
				catch (Exception se)
				{
					LogHelper.LogError(base.GetType(), se);
				}
			}
			return list;
		}

		public List<int> GetCheckedItemIndex(int row_index)
		{
			List<int> list = new List<int>();
			bool flag = this.skinDataGridView.Rows.Count > 0;
			if (flag)
			{
				try
				{
					for (int i = 0; i < this.skinDataGridView.Rows.Count; i++)
					{
						bool flag2 = row_index == i;
						if (!flag2)
						{
							CheckBoxTextCell checkBoxTextCell = (CheckBoxTextCell)this.skinDataGridView.Rows[i].Cells[0];
							bool flag3 = Convert.ToBoolean(checkBoxTextCell.Value);
							bool flag4 = flag3;
							if (flag4)
							{
								list.Add(this.skinDataGridView.Rows[i].Index);
							}
						}
					}
				}
				catch (Exception se)
				{
					LogHelper.LogError(base.GetType(), se);
				}
			}
			return list;
		}

		public void DeleteCheckedItem(List<int> ids)
		{
			try
			{
				for (int i = ids.Count - 1; i >= 0; i--)
				{
					this.skinDataGridView.Rows.RemoveAt(ids[i]);
				}
				this.ResetOffset();
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void ResetOffset()
		{
			this.m_pageIndex = 1;
			this.m_offset = 0;
		}

		public void ClearDataGridView()
		{
			SkinDataGridView expr_07 = this.skinDataGridView;
			if (expr_07 != null)
			{
				expr_07.Rows.Clear();
			}
		}

		public void Calculation(bool isOffset = false)
		{
			try
			{
				if (isOffset)
				{
					this.ResetOffset();
				}
                this.lbl_count.Visible = false;
				this.lbl_count.Text = "共计:" + this.m_allSize + "条";
                this.lbl_count.Location = new Point(this.Width-200, this.Height-100);
                this.lbl_count.Visible = true;
				int num = (this.m_allSize % this.m_pageSize != 0) ? (this.m_allSize / this.m_pageSize + 1) : (this.m_allSize / this.m_pageSize);
				bool flag = num == 0;
				if (flag)
				{
					this.lbl_index.Text = "0/0";
				}
				else
				{
					this.lbl_index.Text = this.m_pageIndex + "/" + num;
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void btn_Click(object sender, EventArgs e)
		{
			int offset = this.m_offset;
			bool flag = sender == this.btn_first;
			if (flag)
			{
				this.m_pageIndex = 1;
				this.m_offset = 0;
			}
			else
			{
				bool flag2 = sender == this.btn_prev;
				if (flag2)
				{
					int num = this.m_offset - this.PageSize;
					this.m_offset = ((num >= 0) ? num : this.m_offset);
					int num2 = this.m_pageIndex - 1;
					this.m_pageIndex = ((num2 > 0) ? num2 : this.m_pageIndex);
				}
				else
				{
					bool flag3 = sender == this.btn_next;
					if (flag3)
					{
						int num3 = (this.m_allSize % this.m_pageSize != 0) ? (this.m_allSize / this.m_pageSize + 1) : (this.m_allSize / this.m_pageSize);
						this.m_offset = ((this.m_pageIndex < num3) ? (this.m_offset + this.PageSize) : this.m_offset);
						this.m_pageIndex = ((this.m_pageIndex < num3) ? (this.m_pageIndex + 1) : this.m_pageIndex);
					}
					else
					{
						bool flag4 = sender == this.btn_end;
						if (flag4)
						{
							int num4 = (this.m_allSize % this.m_pageSize != 0) ? (this.m_allSize / this.m_pageSize + 1) : (this.m_allSize / this.m_pageSize);
							this.m_offset = this.PageSize * (num4 - 1);
							this.m_pageIndex = num4;
						}
					}
				}
			}
			MySplitDataGridView.PaginationBtnClickHandle expr_164 = this.PaginationBtnClicked;
			if (expr_164 != null)
			{
				expr_164(this, new MyEventArgs(this.m_offset));
			}
			try
			{
				bool flag5 = offset.Equals(this.m_offset);
				if (flag5)
				{
					try
					{
						DatagridViewCheckBoxHeaderCell datagridViewCheckBoxHeaderCell = (DatagridViewCheckBoxHeaderCell)this.skinDataGridView.Columns[0].HeaderCell;
						this.CheckBoxSelect(datagridViewCheckBoxHeaderCell.IsChecked);
					}
					catch
					{
					}
				}
				else
				{
					this.CheckBoxSelect(this._isAllSelectBtn);
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void datagrid_MouseClick(object sender, MouseEventArgs e)
		{
			try
			{
				DataGridView dataGridView = (DataGridView)sender;
				DataGridViewRow clickedRow = this.GetClickedRow(dataGridView, e.Y);
				bool flag = clickedRow == null;
				if (flag)
				{
					dataGridView.ClearSelection();
					Action<int> expr_2D = this.onScrollToEnd;
					if (expr_2D != null)
					{
						expr_2D(this.skinDataGridView.Rows.Count);
					}
				}
				else
				{
					bool flag2 = this.skinDataGridView.CurrentRow != null;
					if (flag2)
					{
						int index = this.skinDataGridView.CurrentRow.Index;
						string str = (string)this.skinDataGridView.Rows[index].Tag;
						bool flag3 = this.DataGridViewCheckClicked != null;
						if (flag3)
						{
							this.DataGridViewCheckClicked(sender, new MyEventArgs(str, 0, e));
						}
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public DataGridViewRow GetClickedRow(DataGridView dgv, int mouseY)
		{
			bool flag = dgv.FirstDisplayedScrollingRowIndex < 0;
			DataGridViewRow result;
			if (flag)
			{
				result = null;
			}
			else
			{
				bool flag2 = dgv.ColumnHeadersVisible && mouseY <= dgv.ColumnHeadersHeight;
				if (flag2)
				{
					result = null;
				}
				else
				{
					try
					{
						int num = dgv.FirstDisplayedScrollingRowIndex;
						int num2 = dgv.DisplayedRowCount(true);
						int i = 1;
						while (i <= num2)
						{
							bool visible = dgv.Rows[num].Visible;
							if (visible)
							{
								Rectangle rowDisplayRectangle = dgv.GetRowDisplayRectangle(num, true);
								bool flag3 = rowDisplayRectangle.Top <= mouseY && mouseY < rowDisplayRectangle.Bottom;
								if (flag3)
								{
									result = dgv.Rows[num];
									return result;
								}
								i++;
							}
							num++;
						}
					}
					catch (Exception se)
					{
						LogHelper.LogError(base.GetType(), se);
					}
					result = null;
				}
			}
			return result;
		}

		public void FirstRowClicked()
		{
			try
			{
				bool flag = this.skinDataGridView.Rows.Count > 0;
				if (flag)
				{
					string str = (string)this.skinDataGridView.Rows[0].Tag;
					bool flag2 = this.DataGridViewCheckClicked != null;
					if (flag2)
					{
						this.DataGridViewCheckClicked(this.skinDataGridView, new MyEventArgs(str, 0, EventArgs.Empty));
					}
					DataGridViewRow dataGridViewRow = this.skinDataGridView.Rows[0];
					dataGridViewRow.Selected = true;
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void skinDataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
		{
			try
			{
				object tag = this.skinDataGridView.Rows.SharedRow(e.RowIndex).Tag;
				bool flag = tag != null;
				if (flag)
				{
					int num = this.skinDataGridView.Rows.SharedRow(e.RowIndex).Cells[0].OwningColumn.Width;
					string[] array = tag.ToString().Split(new char[]
					{
						','
					});
					bool flag2 = array.Count<string>() == 3;
					if (flag2)
					{
						string a = array[2];
						bool flag3 = a == 1.ToString();
						if (flag3)
						{
							/*using (Image point = Resources.point)
							{
								e.Graphics.DrawImage(point, new Point(num >> 4, e.RowBounds.Y + 8));
							}*/
						}
					}
					else
					{
						bool flag4 = array.Count<string>() == 5 || array.Count<string>() == 7;
						if (flag4)
						{
							string value = array[3];
							int num2 = 5;
							bool flag5 = !string.IsNullOrEmpty(value);
							if (flag5)
							{
								double num3 = Convert.ToDouble(value);
								bool flag6 = num3 <= 120.0;
								if (flag6)
								{
									/*using (Image 在线 = Resources.在线)
									{
										e.Graphics.DrawImage(在线, new Point(num >> num2, e.RowBounds.Y + 14));
									}*/
								}
								else
								{
									/*using (Image 离线 = Resources.离线)
									{
										e.Graphics.DrawImage(离线, new Point(num >> num2, e.RowBounds.Y + 14));
									}*/
								}
							}
							else
							{
								/*using (Image 离线2 = Resources.离线)
								{
									e.Graphics.DrawImage(离线2, new Point(num >> num2, e.RowBounds.Y + 14));
								}*/
							}
						}
						else
						{
							bool flag7 = array.Count<string>() == 6;
							if (flag7)
							{
								bool flag8 = this.skinDataGridView != null && this.skinDataGridView.Columns != null && this.skinDataGridView.Columns.Count >= 7;
								if (flag8)
								{
									bool flag9 = "等待执行".Equals(array[5]);
									if (flag9)
									{
										int width = this.skinDataGridView.Rows.SharedRow(e.RowIndex).Cells[5].OwningColumn.Width;
										num = width + this.skinDataGridView.Rows.SharedRow(e.RowIndex).Cells[6].OwningColumn.Width;
										num = this.skinDataGridView.Width - num;
										bool flag10 = this.tempImage == null;
										if (flag10)
										{
											//this.tempImage = Resources.ongoing;
										}
										bool flag11 = width > this.tempImage.Width;
										if (flag11)
										{
											num += (width - this.tempImage.Width) / 2;
										}
										e.Graphics.DrawImage(this.tempImage, new Point(num, e.RowBounds.Y + 14));
										this.timer_gif.Enabled = true;
										this.skinDataGridView.Rows.SharedRow(e.RowIndex).Cells[6].Style.ForeColor = ColorUtil.Gray;
									}
									else
									{
										this.skinDataGridView.Rows.SharedRow(e.RowIndex).Cells[5].Style.ForeColor = Color.Red;
										this.skinDataGridView.Rows.SharedRow(e.RowIndex).Cells[6].Style.ForeColor = ColorUtil.DeepBlue;
									}
								}
							}
						}
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void skinDataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
		{
			this.skinDataGridView.ClearSelection();
		}

		private void skinDataGridView1_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
		{
			try
			{
				bool flag = e.ColumnIndex != -1 && this.skinDataGridView.Columns[e.ColumnIndex].Tag != null;
				if (flag)
				{
					this.Cursor = Cursors.Hand;
				}
				else
				{
					bool flag2 = this._isCheckMouseState && e.RowIndex != -1;
					if (flag2)
					{
						object tag = this.skinDataGridView.Rows[e.RowIndex].Tag;
						bool flag3 = tag != null;
						if (flag3)
						{
							string[] array = tag.ToString().Split(new char[]
							{
								','
							});
							bool flag4 = array.Count<string>() == 6;
							if (flag4)
							{
								this._IsShowButton = !"等待执行".Equals(array[5]);
							}
						}
					}
					bool flag5 = e.ColumnIndex == this.buttonIndex && this._IsShowButton && e.RowIndex != -1;
					if (flag5)
					{
						this.Cursor = Cursors.Hand;
					}
					else
					{
						this.Cursor = Cursors.Default;
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void skinDataGridView1_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
		{
			this.Cursor = Cursors.Default;
		}

		private void skinDataGridView1_KeyDown(object sender, KeyEventArgs e)
		{
			bool flag = e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up || e.KeyCode == Keys.Down;
			if (flag)
			{
				e.Handled = true;
			}
		}

		private void btn_allSelect_Click(object sender, EventArgs e)
		{
			bool flag = "全选".Equals(this.btn_allSelect.Text);
			if (flag)
			{
				this.btn_allSelect.Text = "取消全选";
				this.AllSelectBtnCheckState = true;
				this.CheckBoxSelect(true);
			}
			else
			{
				bool flag2 = "取消全选".Equals(this.btn_allSelect.Text);
				if (flag2)
				{
					this.btn_allSelect.Text = "全选";
					this.AllSelectBtnCheckState = false;
					this.CheckBoxSelect(false);
				}
			}
		}

		public void cancelAllSelect()
		{
			bool allSelectBtnShowState = this.AllSelectBtnShowState;
			if (allSelectBtnShowState)
			{
				this.AllSelectBtnText = "全选";
				this.AllSelectBtnCheckState = false;
			}
			this.CancelSelectedForHeader(false);
		}

		private void skinDataGridView_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
		{
			try
			{
				int num = 2;
				bool flag = e.RowIndex >= 0 && e.ColumnIndex == num;
				if (flag)
				{
					object tag = this.skinDataGridView.Rows.SharedRow(e.RowIndex).Tag;
					bool flag2 = tag == null;
					if (!flag2)
					{
						bool flag3 = this.skinDataGridView.Rows[e.RowIndex].Cells[num].Value == DBNull.Value;
						if (!flag3)
						{
							string[] array = tag.ToString().Split(new char[]
							{
								','
							});
							bool flag4 = array.Count<string>() == 5 && !string.IsNullOrEmpty(array[4]);
							if (flag4)
							{
								e.CellStyle.ForeColor = Color.Red;
								this.skinDataGridView.Rows[e.RowIndex].Cells[num].ToolTipText = array[4];
							}
						}
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void timer_gif_Tick(object sender, EventArgs e)
		{
			try
			{
				bool flag = this.tempImage == null;
				if (flag)
				{
					//this.tempImage = Resources.ongoing;
				}
				FrameDimension dimension = new FrameDimension(this.tempImage.FrameDimensionsList[0]);
				int frameCount = this.tempImage.GetFrameCount(dimension);
				this.gifFrameIndex++;
				bool flag2 = this.gifFrameIndex >= frameCount;
				if (flag2)
				{
					this.gifFrameIndex = 0;
				}
				this.tempImage.SelectActiveFrame(dimension, this.gifFrameIndex);
				bool flag3 = this.skinDataGridView != null && this.skinDataGridView.Rows != null && this.skinDataGridView.Rows.Count > 0;
				if (flag3)
				{
					int i = 0;
					while (i < this.skinDataGridView.Rows.Count)
					{
						object tag = this.skinDataGridView.Rows[i].Tag;
						bool flag4 = tag != null;
						if (flag4)
						{
							string[] array = tag.ToString().Split(new char[]
							{
								','
							});
							bool flag5 = array.Count<string>() >= 6;
							if (flag5)
							{
								bool flag6 = this.skinDataGridView != null && this.skinDataGridView.Columns != null && this.skinDataGridView.Columns.Count >= 7;
								if (flag6)
								{
									bool flag7 = "等待执行".Equals(array[5]);
									if (flag7)
									{
										int width = this.skinDataGridView.Rows[i].Cells[5].OwningColumn.Width;
										int num = width + this.skinDataGridView.Rows[i].Cells[6].OwningColumn.Width;
										num = this.skinDataGridView.Width - num;
										bool flag8 = this.tempImage == null;
										if (flag8)
										{
											//this.tempImage = Resources.ongoing;
										}
										bool flag9 = width > this.tempImage.Width;
										if (flag9)
										{
											num += (width - this.tempImage.Width) / 2;
										}
										int y = this.skinDataGridView.GetCellDisplayRectangle(0, i, false).Y;
										bool flag10 = y < 50 || y >= this.skinDataGridView.Height;
										if (!flag10)
										{
											this.skinDataGridView.CreateGraphics().DrawImage(this.tempImage, new Point(num, y + 14));
										}
									}
								}
							}
						}
						IL_273:
						i++;
						continue;
						goto IL_273;
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public void StopGifRefersh()
		{
			this.timer_gif.Enabled = false;
		}

		private void MySplitDataGridView_Load(object sender, EventArgs e)
		{
			bool visible = this.btn_refersh.Visible;
			if (visible)
			{
				this.btn_refersh.Location = new Point(base.Width - 80, 4);
			}
		}

		private void MySplitDataGridView_Paint(object sender, PaintEventArgs e)
		{
			bool visible = this.btn_refersh.Visible;
			if (visible)
			{
				this.btn_refersh.Location = new Point(base.Width - 80, 4);
			}
		}

		private void btn_refersh_Click(object sender, EventArgs e)
		{
			Action expr_07 = this.RefershClick;
			if (expr_07 != null)
			{
				expr_07();
			}
		}

		private void skinDataGridView_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
		{
			try
			{
				int columnIndex = e.ColumnIndex;
				int rowIndex = e.RowIndex;
				bool flag = rowIndex >= 0 && columnIndex == 0;
				if (flag)
				{
					int num = this.GetCheckedItemIndex(rowIndex).Count;
					int num2 = this.RowCounts();
					DataGridViewCell dataGridViewCell = this.skinDataGridView.Rows[rowIndex].Cells[columnIndex];
					bool flag2 = dataGridViewCell.Value == null;
					if (!flag2)
					{
						bool flag3 = (bool)dataGridViewCell.Value;
						bool flag4 = flag3;
						if (flag4)
						{
							num++;
						}
						DatagridViewCheckBoxHeaderCell datagridViewCheckBoxHeaderCell = (DatagridViewCheckBoxHeaderCell)this.skinDataGridView.Columns[columnIndex].HeaderCell;
						bool isChecked = datagridViewCheckBoxHeaderCell.IsChecked;
						bool flag5 = !isChecked;
						if (flag5)
						{
							bool flag6 = num == num2;
							if (flag6)
							{
								this.CancelSelectedForHeader(true);
							}
						}
						else
						{
							bool flag7 = num != num2;
							if (flag7)
							{
								this.CancelSelectedForHeader(false);
							}
						}
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void skinDataGridView_Scroll(object sender, ScrollEventArgs e)
		{
			try
			{
				bool flag = this.onScrollToEnd != null && ScrollOrientation.VerticalScroll.Equals(e.ScrollOrientation);
				if (flag)
				{
					int count = this.skinDataGridView.Rows.Count;
					bool flag2 = count.Equals(e.NewValue + this.skinDataGridView.DisplayedRowCount(false));
					if (flag2)
					{
						Action<int> expr_62 = this.onScrollToEnd;
						if (expr_62 != null)
						{
							expr_62(count);
						}
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public int DisplayedRowCount(bool includePartialRow)
		{
			return this.skinDataGridView.DisplayedRowCount(includePartialRow);
		}

		public void CancelAllSelect()
		{
			bool allSelectBtnShowState = this.AllSelectBtnShowState;
			if (allSelectBtnShowState)
			{
				this.AllSelectBtnText = "全选";
				this.AllSelectBtnCheckState = false;
			}
			this.CancelSelectedForHeader(false);
		}

		protected override void Dispose(bool disposing)
		{
			bool flag = disposing && this.components != null;
			if (flag)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new Container();
			DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle4 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle5 = new DataGridViewCellStyle();
			this.tableLayoutPanel = new TableLayoutPanel();
			this.lbl_index = new SkinLabel();
			this.btn_prev = new MyButton();
			this.btn_next = new MyButton();
			this.btn_end = new MyButton();
			this.btn_first = new MyButton();
			this.lbl_count = new SkinLabel();
			this.skinDataGridView = new SkinDataGridView();
			this.btn_allSelect = new MyButton();
			this.timer_gif = new Timer(this.components);
			this.btn_refersh = new MyButton();
			this.tableLayoutPanel.SuspendLayout();
			((ISupportInitialize)this.skinDataGridView).BeginInit();
			base.SuspendLayout();
			this.tableLayoutPanel.ColumnCount = 7;
			this.tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
			this.tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 30f));
			this.tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 30f));
			this.tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 90f));
			this.tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 30f));
			this.tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 30f));
			this.tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
			this.tableLayoutPanel.Controls.Add(this.lbl_index, 3, 0);
			this.tableLayoutPanel.Controls.Add(this.btn_prev, 2, 0);
			this.tableLayoutPanel.Controls.Add(this.btn_next, 4, 0);
			this.tableLayoutPanel.Controls.Add(this.btn_end, 5, 0);
			this.tableLayoutPanel.Controls.Add(this.btn_first, 1, 0);
			this.tableLayoutPanel.Dock = DockStyle.Bottom;
			this.tableLayoutPanel.Location = new Point(0, 443);
			this.tableLayoutPanel.Margin = new Padding(12, 14, 12, 14);
			this.tableLayoutPanel.Name = "tableLayoutPanel";
			this.tableLayoutPanel.Padding = new Padding(0, 2, 0, 0);
			this.tableLayoutPanel.RowCount = 1;
			this.tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100f));
			this.tableLayoutPanel.Size = new Size(552, 32);
			this.tableLayoutPanel.TabIndex = 8;
			this.lbl_index.BackColor = Color.Transparent;
			this.lbl_index.BorderColor = Color.White;
			this.lbl_index.Dock = DockStyle.Fill;
			this.lbl_index.Font = new Font("微软雅黑", 9f, FontStyle.Regular, GraphicsUnit.Point, 134);
			this.lbl_index.Location = new Point(234, 2);
			this.lbl_index.Name = "lbl_index";
			this.lbl_index.Size = new Size(84, 30);
			this.lbl_index.TabIndex = 6;
			this.lbl_index.Text = "0";
			this.lbl_index.TextAlign = ContentAlignment.MiddleCenter;
			this.btn_prev.Dock = DockStyle.Fill;
			this.btn_prev.EnterForeColor = Color.White;
			this.btn_prev.FlatAppearance.BorderColor = Color.FromArgb(178, 178, 178);
			this.btn_prev.FlatStyle = FlatStyle.Flat;
			this.btn_prev.Font = new Font("微软雅黑", 6f);
			this.btn_prev.LeaveBackColor = Color.White;
			this.btn_prev.LeaveForeColor = Color.Black;
			this.btn_prev.Location = new Point(204, 6);
			this.btn_prev.Margin = new Padding(3, 4, 3, 4);
			this.btn_prev.Name = "btn_prev";
			this.btn_prev.Size = new Size(24, 22);
			this.btn_prev.TabIndex = 8;
			this.btn_prev.TabStop = false;
			this.btn_prev.Text = "<";
			this.btn_prev.Click += new EventHandler(this.btn_Click);
			this.btn_next.Dock = DockStyle.Fill;
			this.btn_next.EnterForeColor = Color.White;
			this.btn_next.FlatAppearance.BorderColor = Color.FromArgb(178, 178, 178);
			this.btn_next.FlatStyle = FlatStyle.Flat;
			this.btn_next.Font = new Font("微软雅黑", 6f);
			this.btn_next.LeaveBackColor = Color.White;
			this.btn_next.LeaveForeColor = Color.Black;
			this.btn_next.Location = new Point(324, 6);
			this.btn_next.Margin = new Padding(3, 4, 3, 4);
			this.btn_next.Name = "btn_next";
			this.btn_next.Size = new Size(24, 22);
			this.btn_next.TabIndex = 9;
			this.btn_next.TabStop = false;
			this.btn_next.Text = ">";
			this.btn_next.Click += new EventHandler(this.btn_Click);
			this.btn_end.Dock = DockStyle.Fill;
			this.btn_end.EnterForeColor = Color.White;
			this.btn_end.FlatAppearance.BorderColor = Color.FromArgb(178, 178, 178);
			this.btn_end.FlatStyle = FlatStyle.Flat;
			this.btn_end.Font = new Font("微软雅黑", 6f);
			this.btn_end.LeaveBackColor = Color.White;
			this.btn_end.LeaveForeColor = Color.Black;
			this.btn_end.Location = new Point(354, 6);
			this.btn_end.Margin = new Padding(3, 4, 3, 4);
			this.btn_end.Name = "btn_end";
			this.btn_end.Size = new Size(24, 22);
			this.btn_end.TabIndex = 10;
			this.btn_end.TabStop = false;
			this.btn_end.Text = ">|";
			this.btn_end.Click += new EventHandler(this.btn_Click);
			this.btn_first.Dock = DockStyle.Fill;
			this.btn_first.EnterForeColor = Color.White;
			this.btn_first.FlatAppearance.BorderColor = Color.FromArgb(178, 178, 178);
			this.btn_first.FlatStyle = FlatStyle.Flat;
			this.btn_first.Font = new Font("微软雅黑", 6f);
			this.btn_first.ForeColor = Color.Black;
			this.btn_first.LeaveBackColor = Color.White;
			this.btn_first.LeaveForeColor = Color.Black;
			this.btn_first.Location = new Point(174, 6);
			this.btn_first.Margin = new Padding(3, 4, 3, 4);
			this.btn_first.Name = "btn_first";
			this.btn_first.Size = new Size(24, 22);
			this.btn_first.TabIndex = 7;
			this.btn_first.TabStop = false;
			this.btn_first.Text = "|<";
			this.btn_first.Click += new EventHandler(this.btn_Click);
			this.lbl_count.BackColor = Color.Transparent;
			this.lbl_count.BorderColor = Color.White;
			this.lbl_count.Dock = DockStyle.Top;
			this.lbl_count.Font = new Font("微软雅黑", 9f, FontStyle.Regular, GraphicsUnit.Point, 134);
			//this.lbl_count.Location = new Point(0, 0);
			this.lbl_count.Margin = new Padding(3, 0, 10, 0);
			this.lbl_count.Name = "lbl_count";
			this.lbl_count.Size = new Size(552, 33);
			this.lbl_count.TabIndex = 1;
			//this.lbl_count.Text = "共计";
			this.lbl_count.TextAlign = ContentAlignment.MiddleLeft;
			this.skinDataGridView.AlternatingCellBackColor = Color.FromArgb(192, 0, 0);
			dataGridViewCellStyle.BackColor = Color.FromArgb(192, 0, 0);
			dataGridViewCellStyle.SelectionBackColor = Color.DarkGray;
			dataGridViewCellStyle.SelectionForeColor = SystemColors.HighlightText;
			this.skinDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle;
			this.skinDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
			this.skinDataGridView.BackgroundColor = Color.White;
            this.skinDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.skinDataGridView.ColumnFont = null;
			this.skinDataGridView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
			dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle2.BackColor = Color.FromArgb(247, 246, 239);
			dataGridViewCellStyle2.Font = new Font("微软雅黑", 10f);
			dataGridViewCellStyle2.ForeColor = SystemColors.WindowText;
			dataGridViewCellStyle2.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = DataGridViewTriState.True;
			this.skinDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
			this.skinDataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.skinDataGridView.ColumnSelectBackColor = Color.FromArgb(158, 212, 255);
			this.skinDataGridView.ColumnSelectForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle3.BackColor = Color.White;
			dataGridViewCellStyle3.Font = new Font("微软雅黑", 9f, FontStyle.Regular, GraphicsUnit.Point, 134);
			dataGridViewCellStyle3.ForeColor = SystemColors.ControlText;
			dataGridViewCellStyle3.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle3.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle3.WrapMode = DataGridViewTriState.False;
			this.skinDataGridView.DefaultCellStyle = dataGridViewCellStyle3;
			this.skinDataGridView.Dock = DockStyle.Fill;
			this.skinDataGridView.EnableHeadersVisualStyles = false;
			this.skinDataGridView.Font = new Font("微软雅黑", 9f, FontStyle.Regular, GraphicsUnit.Point, 134);
			this.skinDataGridView.GridColor = SystemColors.ButtonShadow;
			this.skinDataGridView.HeadFont = new Font("微软雅黑", 10f);
			this.skinDataGridView.HeadSelectForeColor = SystemColors.HighlightText;
			this.skinDataGridView.Location = new Point(0, 33);
			this.skinDataGridView.Margin = new Padding(3, 4, 3, 4);
			this.skinDataGridView.MouseCellBackColor = Color.FromArgb(158, 212, 255);
			this.skinDataGridView.MultiSelect = false;
			this.skinDataGridView.Name = "skinDataGridView";
			this.skinDataGridView.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
			dataGridViewCellStyle4.Alignment = DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle4.BackColor = SystemColors.Control;
			dataGridViewCellStyle4.Font = new Font("微软雅黑", 9f, FontStyle.Regular, GraphicsUnit.Point, 134);
			dataGridViewCellStyle4.ForeColor = SystemColors.WindowText;
			dataGridViewCellStyle4.SelectionBackColor = Color.Red;
			dataGridViewCellStyle4.SelectionForeColor = Color.Gray;
			dataGridViewCellStyle4.WrapMode = DataGridViewTriState.True;
			this.skinDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
			this.skinDataGridView.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			dataGridViewCellStyle5.Alignment = DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle5.ForeColor = SystemColors.WindowText;
			dataGridViewCellStyle5.SelectionBackColor = Color.FromArgb(158, 212, 255);
			dataGridViewCellStyle5.SelectionForeColor = SystemColors.HighlightText;
			this.skinDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle5;
			this.skinDataGridView.RowTemplate.Height = 40;
			this.skinDataGridView.ScrollBars = ScrollBars.Vertical;
			this.skinDataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			this.skinDataGridView.Size = new Size(552, 410);
			this.skinDataGridView.SkinGridColor = SystemColors.ButtonShadow;
			this.skinDataGridView.TabIndex = 2;
			this.skinDataGridView.TitleBack = null;
			this.skinDataGridView.TitleBackColorBegin = Color.White;
			this.skinDataGridView.TitleBackColorEnd = Color.FromArgb(83, 196, 242);
			this.skinDataGridView.CellMouseLeave += new DataGridViewCellEventHandler(this.skinDataGridView1_CellMouseLeave);
			this.skinDataGridView.CellMouseMove += new DataGridViewCellMouseEventHandler(this.skinDataGridView1_CellMouseMove);
			this.skinDataGridView.CellMouseUp += new DataGridViewCellMouseEventHandler(this.skinDataGridView_CellMouseUp);
			this.skinDataGridView.CellPainting += new DataGridViewCellPaintingEventHandler(this.skinDataGridView_CellPainting);
			this.skinDataGridView.RowPostPaint += new DataGridViewRowPostPaintEventHandler(this.skinDataGridView1_RowPostPaint);
			this.skinDataGridView.RowsAdded += new DataGridViewRowsAddedEventHandler(this.skinDataGridView1_RowsAdded);
			this.skinDataGridView.Scroll += new ScrollEventHandler(this.skinDataGridView_Scroll);
			this.skinDataGridView.KeyDown += new KeyEventHandler(this.skinDataGridView1_KeyDown);
			this.skinDataGridView.MouseClick += new MouseEventHandler(this.datagrid_MouseClick);
			this.btn_allSelect.BackColor = Color.White;
			this.btn_allSelect.EnterBackColor = Color.FromArgb(178, 178, 178);
			this.btn_allSelect.EnterForeColor = Color.Black;
			this.btn_allSelect.FlatAppearance.BorderColor = Color.FromArgb(178, 178, 178);
			this.btn_allSelect.FlatStyle = FlatStyle.Flat;
			this.btn_allSelect.Font = new Font("微软雅黑", 9f, FontStyle.Regular, GraphicsUnit.Point, 134);
			this.btn_allSelect.ForeColor = Color.Black;
			this.btn_allSelect.LeaveBackColor = Color.White;
			this.btn_allSelect.LeaveForeColor = Color.Black;
			this.btn_allSelect.Location = new Point(94, 4);
			this.btn_allSelect.Name = "btn_allSelect";
			this.btn_allSelect.Size = new Size(75, 25);
			this.btn_allSelect.TabIndex = 9;
			this.btn_allSelect.Text = "全选";
			this.btn_allSelect.UseVisualStyleBackColor = false;
			this.btn_allSelect.Click += new EventHandler(this.btn_allSelect_Click);
			this.timer_gif.Tick += new EventHandler(this.timer_gif_Tick);
			this.btn_refersh.BackColor = Color.White;
			this.btn_refersh.EnterBackColor = Color.FromArgb(178, 178, 178);
			this.btn_refersh.EnterForeColor = Color.Black;
			this.btn_refersh.FlatAppearance.BorderColor = Color.FromArgb(178, 178, 178);
			this.btn_refersh.FlatStyle = FlatStyle.Flat;
			this.btn_refersh.Font = new Font("微软雅黑", 9f, FontStyle.Regular, GraphicsUnit.Point, 134);
			this.btn_refersh.ForeColor = Color.Black;
			//this.btn_refersh.Image = Resources.refersh;
			this.btn_refersh.ImageAlign = ContentAlignment.TopRight;
			this.btn_refersh.LeaveBackColor = Color.White;
			this.btn_refersh.LeaveForeColor = Color.Black;
			this.btn_refersh.Location = new Point(477, 4);
			this.btn_refersh.Name = "btn_refersh";
			this.btn_refersh.Size = new Size(75, 25);
			this.btn_refersh.TabIndex = 9;
			this.btn_refersh.Text = "  刷新";
			this.btn_refersh.TextImageRelation = TextImageRelation.ImageBeforeText;
			this.btn_refersh.UseVisualStyleBackColor = false;
			this.btn_refersh.Visible = false;
			this.btn_refersh.Click += new EventHandler(this.btn_refersh_Click);
			base.AutoScaleDimensions = new SizeF(7f, 17f);
            base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.BackColor = Color.White;
			base.Controls.Add(this.btn_refersh);
			base.Controls.Add(this.btn_allSelect);
			base.Controls.Add(this.skinDataGridView);
			base.Controls.Add(this.tableLayoutPanel);
			base.Controls.Add(this.lbl_count);
			this.DoubleBuffered = true;
			this.Font = new Font("微软雅黑", 9f, FontStyle.Regular, GraphicsUnit.Point, 134);
			base.Margin = new Padding(3, 4, 3, 4);
			base.Name = "MySplitDataGridView";
			base.Size = new Size(552, 475);
			base.Load += new EventHandler(this.MySplitDataGridView_Load);
			base.Paint += new PaintEventHandler(this.MySplitDataGridView_Paint);
			this.tableLayoutPanel.ResumeLayout(false);
			((ISupportInitialize)this.skinDataGridView).EndInit();
			base.ResumeLayout(false);
		}
	}
}
