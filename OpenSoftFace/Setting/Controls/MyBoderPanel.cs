using CommonConstant;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Utils;

namespace OpenSoftFace.Setting.Controls
{
	public class MyBoderPanel : Panel
	{
		private Color _borderColor = ColorUtil.Black;

		private int _borderWidth = 1;

		[Category("Appearance"), Description("组件的边框颜色。")]
		public Color BorderColor
		{
			get
			{
				return this._borderColor;
			}
			set
			{
				this._borderColor = value;
				base.Invalidate();
			}
		}

		[Category("Appearance"), Description("组件的边框宽度。")]
		public int BorderWidth
		{
			get
			{
				return this._borderWidth;
			}
			set
			{
				this._borderWidth = value;
				base.Invalidate();
			}
		}

		public MyBoderPanel()
		{
			base.SetStyle(ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer | ControlStyles.OptimizedDoubleBuffer, true);
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			bool flag = base.BorderStyle == BorderStyle.FixedSingle;
			if (flag)
			{
				IntPtr windowDC = Win32.GetWindowDC(base.Handle);
				Graphics graphics = Graphics.FromHdc(windowDC);
				ControlPaint.DrawBorder(graphics, new Rectangle(0, 0, base.Width, base.Height), this._borderColor, this._borderWidth, ButtonBorderStyle.Solid, this._borderColor, this._borderWidth, ButtonBorderStyle.Solid, this._borderColor, this._borderWidth, ButtonBorderStyle.Solid, this._borderColor, this._borderWidth, ButtonBorderStyle.Solid);
				graphics.Dispose();
				Win32.ReleaseDC(base.Handle, windowDC);
			}
		}
	}
}
