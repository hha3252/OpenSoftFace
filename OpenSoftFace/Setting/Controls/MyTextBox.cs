using CommonConstant;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using Utils;

namespace OpenSoftFace.Setting.Controls
{
	public class MyTextBox : TextBox
	{
		private Font _defaultFont = new Font("微软雅黑", 9f);

		private string _emptyTextTip;

		private Color _emptyTextTipColor = ColorUtil.DarkGray;

		private bool _HotTrack = true;

		private Color _BorderColor = ColorUtil.Black;

		private Color _HotColor = ColorUtil.AirBlue;

		private bool _IsMouseOver = false;

		[Description("当Text属性为空时编辑框内出现的提示文本")]
		public string EmptyTextTip
		{
			get
			{
				return this._emptyTextTip;
			}
			set
			{
				bool flag = this._emptyTextTip != value;
				if (flag)
				{
					this._emptyTextTip = value;
					base.Invalidate();
				}
			}
		}

		[Description("获取或设置EmptyTextTip的颜色")]
		public Color EmptyTextTipColor
		{
			get
			{
				return this._emptyTextTipColor;
			}
			set
			{
				bool flag = this._emptyTextTipColor != value;
				if (flag)
				{
					this._emptyTextTipColor = value;
					base.Invalidate();
				}
			}
		}

		[Category("行为"), DefaultValue(true), Description("获得或设置一个值，指示当鼠标经过控件时控件边框是否发生变化。只在控件的BorderStyle为FixedSingle时有效")]
		public bool HotTrack
		{
			get
			{
				return this._HotTrack;
			}
			set
			{
				this._HotTrack = value;
				base.Invalidate();
			}
		}

		[Category("外观"), DefaultValue(typeof(Color), "#A7A6AA"), Description("获得或设置控件的边框颜色")]
		public Color BorderColor
		{
			get
			{
				return this._BorderColor;
			}
			set
			{
				this._BorderColor = value;
				base.Invalidate();
			}
		}

		[Category("外观"), DefaultValue(typeof(Color), "#335EA8"), Description("获得或设置当鼠标经过控件时控件的边框颜色。只在控件的BorderStyle为FixedSingle时有效")]
		public Color HotColor
		{
			get
			{
				return this._HotColor;
			}
			set
			{
				this._HotColor = value;
				base.Invalidate();
			}
		}

		public MyTextBox()
		{
			this.setStyles();
			this.Font = this._defaultFont;
			base.BorderStyle = BorderStyle.None;
		}

		protected override void WndProc(ref Message m)
		{
			base.WndProc(ref m);
			bool flag = m.Msg == 15 || m.Msg == 307;
			if (flag)
			{
				this.wmPaint(ref m);
				IntPtr windowDC = Win32.GetWindowDC(m.HWnd);
				bool flag2 = windowDC.ToInt32() == 0;
				if (!flag2)
				{
					bool flag3 = base.BorderStyle == BorderStyle.FixedSingle;
					if (flag3)
					{
						Pen pen = new Pen(this._BorderColor, 1f);
						bool hotTrack = this._HotTrack;
						if (hotTrack)
						{
							bool focused = this.Focused;
							if (focused)
							{
								pen.Color = this._HotColor;
							}
							else
							{
								bool isMouseOver = this._IsMouseOver;
								if (isMouseOver)
								{
									pen.Color = this._HotColor;
								}
								else
								{
									pen.Color = this._BorderColor;
								}
							}
						}
						Graphics graphics = Graphics.FromHdc(windowDC);
						graphics.SmoothingMode = SmoothingMode.AntiAlias;
						graphics.DrawRectangle(pen, 0, 0, base.Width - 1, base.Height - 1);
						pen.Dispose();
					}
					m.Result = IntPtr.Zero;
					Win32.ReleaseDC(m.HWnd, windowDC);
				}
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				bool flag = this._defaultFont != null;
				if (flag)
				{
					this._defaultFont.Dispose();
				}
			}
			this._defaultFont = null;
			base.Dispose(disposing);
		}

		private void setStyles()
		{
			base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			base.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
			base.SetStyle(ControlStyles.ResizeRedraw, true);
			base.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
			base.UpdateStyles();
		}

		private void wmPaint(ref Message m)
		{
			Graphics dc = Graphics.FromHwnd(base.Handle);
			bool flag = this.Text.Length == 0 && !string.IsNullOrEmpty(this.EmptyTextTip) && !this.Focused;
			if (flag)
			{
				TextRenderer.DrawText(dc, this.EmptyTextTip, this.Font, base.ClientRectangle, this.EmptyTextTipColor, MyTextBox.getTextFormatFlags(base.TextAlign, this.RightToLeft == RightToLeft.Yes));
			}
		}

		private static TextFormatFlags getTextFormatFlags(HorizontalAlignment alignment, bool rightToleft)
		{
			TextFormatFlags textFormatFlags = TextFormatFlags.SingleLine | TextFormatFlags.WordBreak;
			if (rightToleft)
			{
				textFormatFlags |= (TextFormatFlags.Right | TextFormatFlags.RightToLeft);
			}
			switch (alignment)
			{
			case HorizontalAlignment.Left:
				textFormatFlags |= TextFormatFlags.VerticalCenter;
				break;
			case HorizontalAlignment.Right:
				textFormatFlags |= (TextFormatFlags.Right | TextFormatFlags.VerticalCenter);
				break;
			case HorizontalAlignment.Center:
				textFormatFlags |= (TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter);
				break;
			}
			return textFormatFlags;
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			this._IsMouseOver = true;
			bool hotTrack = this._HotTrack;
			if (hotTrack)
			{
				base.Invalidate();
			}
			base.OnMouseMove(e);
		}

		protected override void OnMouseLeave(EventArgs e)
		{
			this._IsMouseOver = false;
			bool hotTrack = this._HotTrack;
			if (hotTrack)
			{
				base.Invalidate();
			}
			base.OnMouseLeave(e);
		}

		protected override void OnGotFocus(EventArgs e)
		{
			bool hotTrack = this._HotTrack;
			if (hotTrack)
			{
				base.Invalidate();
			}
			base.OnGotFocus(e);
		}

		protected override void OnLostFocus(EventArgs e)
		{
			bool hotTrack = this._HotTrack;
			if (hotTrack)
			{
				base.Invalidate();
			}
			base.OnLostFocus(e);
		}
	}
}
