﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Windows.Forms;
using Model.Entity;
using DAL.Service;
using DAL.Service.Impl;
using Utils;
using OpenSoftFace.Setting.Common;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace OpenSoftFace.WebService
{
    public class Api : IHttpHandler
    {
        private StringBuilder sqlBuilder = new StringBuilder();
        private IMainInfoService personInfoService;
        private IAttendanceRecordService attendanceRecordService;
        private DateTime search_start;
        private DateTime search_end;
        private DateTime tempDateTime = DateTime.Now;
        public void ProcessRequest(HttpContext context)
        {
            var _array = ParseQueryString("http://"+WebHttpService.severIPAddress+context.Request.Url);
            if (_array.ContainsKey("start")) { 
                search_start=Convert.ToDateTime(_array["start"]);
            }
            if (_array.ContainsKey("end"))
            {
                search_end = Convert.ToDateTime(_array["end"]);
            }
            if (_array.ContainsKey("month") && _array.ContainsKey("year"))
            {
                search_start = DateTimeUtil.getMonthFirstDay(Convert.ToDateTime(_array["year"] + "-" + _array["month"] + "-1"));
                search_end = DateTimeUtil.getMonthLastDay(Convert.ToDateTime(_array["year"] + "-" + _array["month"] + "-1"));
            }

            StringBuilder sbText = new StringBuilder();
            if (context.Request.Url.Contains("acion=excel")) {
               String file= excel();
               context.Response.Body = File.ReadAllBytes(file);
               context.Response.StateCode = "200";
               context.Response.ContentType = "multipart/form-data";
               context.Response.StateDescription = "OK";
               context.Response.ContentDisposition = "attachment;fileName=" + file;
               return;
            }
            sbText.Append("Api test!");
            context.Response.Body = Encoding.UTF8.GetBytes(sbText.ToString());
            context.Response.StateCode = "200";
            context.Response.ContentType = "text/html";
            context.Response.StateDescription = "OK";
        }

        public String excel() {
                this.personInfoService = new MainInfoServiceImpl();
                this.attendanceRecordService = new AttendanceRecordServiceImpl();
                string[] where = string.IsNullOrEmpty(this.sqlBuilder.ToString()) ? null : this.sqlBuilder.ToString().Split(new char[]
				{
					','
				});
                List<MainInfo> tempExportList = this.personInfoService.QueryForList(where, null, null, " id desc ", -1, -1);
                bool flag = tempExportList == null || tempExportList.Count <= 0;
                if (flag)
                {
                    return "";
                   /* base.Invoke(new Action(delegate
                    {
                        ErrorDialogForm.ShowErrorMsg("没有任何数据可以导出到Excel文件!", "", false, this, false);
                    }));*/
                }
                else
                {
                    bool flag2 = tempExportList != null && tempExportList.Count > 0;
                    if (flag2)
                    {
                        //MyFolderBrowserDialog myFolderBrowserDialog = new MyFolderBrowserDialog();
                        //DialogResult dialogResult = myFolderBrowserDialog.ShowDialog(this);
                        //bool flag3 = dialogResult == DialogResult.OK;
                        //if (flag3)
                        //{
                            //Action expr_E7 = this.ShowLayer;
                            //if (expr_E7 != null)
                            //{
                                //expr_E7();
                            //}
                            string dir = "./";
                            Dictionary<string, List<DateTime>> dateTimeDict = new Dictionary<string, List<DateTime>>();
                            Dictionary<string, List<string>> columnsDict = DateTimeUtil.getDayDict(this.search_start, this.search_end, ref dateTimeDict, "MM-dd");
                            //ThreadPool.QueueUserWorkItem(new WaitCallback(delegate
                            //{
                                string arg = string.Format(this.search_start.ToString("yyyy年MM")+"月考勤记录{0}.xls", DateTime.Now.ToString("yyyyMMdd_HHmmss"));
                                List<string> list = new List<string>();
                                list.Insert(0, "工号");
                                list.Insert(0, "姓名");
                                list.Insert(0, "");
                                Dictionary<int, string> dictionary = new Dictionary<int, string>();
                                Dictionary<string, List<MainInfo>> dictionary2 = new Dictionary<string, List<MainInfo>>();
                                bool flag4 = dateTimeDict == null || dateTimeDict.Count <= 0;
                                if (flag4)
                                {
                                   // this.Invoke(new Action(() => this.exportBtn_Click(null, null)));
                                }
                                foreach (string current in columnsDict.Keys)
                                {
                                    List<MainInfo> list2 = new List<MainInfo>();
                                    list2.AddRange(tempExportList);
                                    List<DateTime> list3 = dateTimeDict[current];
                                    bool flag5 = list3 != null && list3.Count > 0;
                                    if (flag5)
                                    {
                                        dictionary2.Add(current, this.getExportAttendRecord2List(list2, list3[0], list3[list3.Count - 1]));
                                    }
                                }
                                bool flag6 = dictionary2 != null && dictionary2.Count > 0;
                                if (flag6)
                                {
                                    try
                                    {
                                        bool flag7 = ExcelOperate.AttendRecordsToExcel(list, columnsDict, dictionary2, string.Format("{0}/{1}", dir, arg), this.search_start.ToString("yyyy年MM月"));
                                        return arg;
                                        bool flag8 = flag7;
                                        if (flag8)
                                        {
                                            return "";
                                            //this.Invoke(new Action(() => this.exportBtn_Click(null,null)));
                                            //MessageBox.Show(arg + "保存成功！");
                                            //ErrorDialogForm.ShowErrorMsg(arg + "保存成功！", "", true, this, true);
                                            //UpdateSuccessDialogForm.showMsg(arg + "保存成功！", "", false, null, false);
                                        }
                                        else
                                        {
                                            return "";
                                            //this.Invoke(new Action(() => this.exportBtn_Click(null, null)));
                                        }
                                    }
                                    catch (Exception se2)
                                    {
                                        return "";
                                       // LogHelper.LogError(this.GetType(), se2);
                                        //this.Invoke(new Action(() => this.exportBtn_Click(null, null)));
                                    }
                                }
                                else
                                {
                                    return "";
                                    //this.Invoke(new Action(() => this.exportBtn_Click(null, null)));
                                }
                           // }));
                        //}
                    }
                    else
                    {
                        return "";
                        //ErrorDialogForm.ShowErrorMsg("查询数据为空,请选择其他查询条件重新导出!", "", false, null, false);
                    }
                }
        }


        private List<MainInfo> getExportAttendRecord2List(List<MainInfo> personInfoList, DateTime startTime, DateTime endTime)
        {
            List<MainInfo> list = new List<MainInfo>();
            List<MainInfo> result;
            try
            {
                bool flag = personInfoList == null || personInfoList.Count <= 0;
                if (flag)
                {
                    result = list;
                    return result;
                }
                //this.search_start = this.date_time_start.Value;
                //this.search_end = this.date_time_end.Value;
                string text = string.Empty;
                bool flag2 = personInfoList.Count < 1000;
                if (flag2)
                {
                    string text2 = string.Empty;
                    StringBuilder personIdBuilder = new StringBuilder();
                    personInfoList.ForEach(delegate(MainInfo a)
                    {
                        personIdBuilder.Append(a.ID + ",");
                    });
                    bool flag3 = personIdBuilder.Length > 0;
                    if (flag3)
                    {
                        text2 = personIdBuilder.ToString().Substring(0, personIdBuilder.Length - 1);
                    }
                    text = string.Format(" {2}(datetime(date) >= datetime('{0}') and datetime(date) <= datetime('{1}'))", startTime.ToString("yyyy-MM-dd"), endTime.ToString("yyyy-MM-dd"), string.IsNullOrWhiteSpace(text2) ? string.Format("(id in ({0}) and ", text2) : string.Empty);
                }
                else
                {
                    text = string.Format(" (datetime(date) >= datetime('{0}') and datetime(date) <= datetime('{1}'))", startTime.ToString("yyyy-MM-dd"), endTime.ToString("yyyy-MM-dd"));
                }
                Dictionary<string, AttendanceRecord2> dictionary = this.attendanceRecordService.QueryForAttendRecord(new string[]
				{
					text
				}, null, null, null, -1, -1);
                List<string> dayList = DateTimeUtil.getDayList(startTime, endTime, "", "yyyy/MM/dd");
                foreach (MainInfo current in personInfoList)
                {
                    List<AttendanceRecord2> list2 = new List<AttendanceRecord2>();
                    int num = 0;
                    int num2 = 0;
                    int num3 = 0;
                    DateTime now = DateTime.Now;
                    DateTime now2 = DateTime.Now;
                    bool flag4 = dayList != null && dayList.Count > 0 && DateTime.TryParse(current.AddTime, out now) && DateTime.TryParse(current.UpdateTime, out now2);
                    if (flag4)
                    {
                        foreach (string current2 in dayList)
                        {
                            AttendanceRecord2 attendanceRecord = new AttendanceRecord2();
                            string key = string.Format("{0}${1}", current.ID, current2);
                            bool flag5 = dictionary.ContainsKey(key);
                            if (flag5)
                            {
                                attendanceRecord = dictionary[key];
                            }
                            else
                            {
                                attendanceRecord.AttendStatus = 0;
                            }
                            attendanceRecord.WeekText = DateTimeUtil.Week(DateTime.Parse(current2));
                            attendanceRecord.AttendStatusText = "--";
                            int attendStatus = attendanceRecord.AttendStatus;
                            if (attendStatus != 1)
                            {
                                if (attendStatus != 2)
                                {
                                    attendanceRecord.AttendStatusText = "--";
                                }
                                else
                                {
                                    num3++;
                                    num++;
                                    attendanceRecord.AttendStatusText = "正常";
                                }
                            }
                            else
                            {
                                num3++;
                                num2++;
                                attendanceRecord.AttendStatusText = "缺勤";
                            }
                            attendanceRecord.Date = current2;
                            bool flag6 = this.isCalculateAttendance(current, now, now2, current2);
                            if (flag6)
                            {
                                list2.Add(attendanceRecord);
                            }
                        }
                    }
                    current.MonthAttendTotal = num.ToString();
                    current.MonthAbsenceTotal = num2.ToString();
                    current.MonthTotal = num3.ToString();
                    MainInfo mainInfo = (MainInfo)CommonUtil.CopyOjbect(current);
                    mainInfo.attendRecordList = list2;
                    bool flag7 = list2 != null && list2.Count > 0;
                    if (flag7)
                    {
                        list.Add(mainInfo);
                    }
                }
            }
            catch (Exception se)
            {
                //LogHelper.LogError(base.GetType(), se);
            }
            result = list;
            return result;
        }

        private bool isCalculateAttendance(MainInfo mainInfo, DateTime addDate, DateTime updateDate, string currentDate)
        {
            bool flag = false;
            bool result;
            try
            {
                bool flag2 = !DateTime.TryParse(currentDate, out this.tempDateTime);
                if (flag2)
                {
                    result = flag;
                    return result;
                }
                DateTime monthFirstDay = DateTimeUtil.getMonthFirstDay(this.tempDateTime);
                DateTime monthLastDay = DateTimeUtil.getMonthLastDay(this.tempDateTime);
                bool flag3 = mainInfo.PValid == -1;
                if (flag3)
                {
                    bool flag4 = DateTime.Compare(addDate, monthLastDay) <= 0 && DateTime.Compare(updateDate, monthFirstDay) >= 0;
                    if (flag4)
                    {
                        flag = true;
                    }
                }
                else
                {
                    bool flag5 = DateTime.Compare(addDate, monthLastDay) <= 0;
                    if (flag5)
                    {
                        result = true;
                        return result;
                    }
                }
            }
            catch (Exception se)
            {
               // LogHelper.LogError(base.GetType(), se);
            }
            result = flag;
            return result;
        }

        public static Dictionary<string, string> ParseQueryString(string url)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                throw new ArgumentNullException("url");
            }
            var uri = new Uri(url);
            if (string.IsNullOrWhiteSpace(uri.Query))
            {
                return new Dictionary<string, string>();
            }
            //1.去除第一个前导?字符
            var dic = uri.Query.Substring(1)
                //2.通过&划分各个参数
                    .Split(new char[] { '&' }, StringSplitOptions.RemoveEmptyEntries)
                //3.通过=划分参数key和value,且保证只分割第一个=字符
                    .Select(param => param.Split(new char[] { '=' }, 2, StringSplitOptions.RemoveEmptyEntries))
                //4.通过相同的参数key进行分组
                    .GroupBy(part => part[0], part => part.Length > 1 ? part[1] : string.Empty)
                //5.将相同key的value以,拼接
                    .ToDictionary(group => group.Key, group => string.Join(",", group));

            return dic;
        }

    }
}
