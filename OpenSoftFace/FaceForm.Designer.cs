﻿namespace OpenSoftFace
{
    partial class FaceForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FaceForm));
            this.picImageCompare = new System.Windows.Forms.PictureBox();
            this.chooseMultiImgBtn = new System.Windows.Forms.Button();
            this.logBox = new System.Windows.Forms.TextBox();
            this.chooseImgBtn = new System.Windows.Forms.Button();
            this.imageLists = new System.Windows.Forms.ImageList(this.components);
            this.matchBtn = new System.Windows.Forms.Button();
            this.btnClearFaceList = new System.Windows.Forms.Button();
            this.lblCompareImage = new System.Windows.Forms.Label();
            this.lblCompareInfo = new System.Windows.Forms.Label();
            this.rgbVideoSource = new AForge.Controls.VideoSourcePlayer();
            this.btnStartVideo = new System.Windows.Forms.Button();
            this.txtThreshold = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.irVideoSource = new AForge.Controls.VideoSourcePlayer();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox_close = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.updateTime = new System.Windows.Forms.Timer(this.components);
            this.imageList = new System.Windows.Forms.ListView();
            this.lab_hhmm = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pic_max = new System.Windows.Forms.PictureBox();
            this.pic_logo = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pic_set = new System.Windows.Forms.PictureBox();
            this.pan_kq = new System.Windows.Forms.Panel();
            this.pic_facex = new OpenSoftFace.iControls.CirclePictureBox();
            this.lab_date = new System.Windows.Forms.Label();
            this.pic_face = new System.Windows.Forms.PictureBox();
            this.lab_name = new System.Windows.Forms.Label();
            this.lab_dept = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picImageCompare)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_close)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_logo)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_set)).BeginInit();
            this.pan_kq.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_facex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_face)).BeginInit();
            this.SuspendLayout();
            // 
            // picImageCompare
            // 
            this.picImageCompare.BackColor = System.Drawing.Color.Black;
            this.picImageCompare.Location = new System.Drawing.Point(480, 210);
            this.picImageCompare.Name = "picImageCompare";
            this.picImageCompare.Size = new System.Drawing.Size(88, 22);
            this.picImageCompare.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picImageCompare.TabIndex = 1;
            this.picImageCompare.TabStop = false;
            this.picImageCompare.Visible = false;
            // 
            // chooseMultiImgBtn
            // 
            this.chooseMultiImgBtn.Location = new System.Drawing.Point(481, 590);
            this.chooseMultiImgBtn.Name = "chooseMultiImgBtn";
            this.chooseMultiImgBtn.Size = new System.Drawing.Size(46, 42);
            this.chooseMultiImgBtn.TabIndex = 32;
            this.chooseMultiImgBtn.Text = "设置";
            this.chooseMultiImgBtn.UseVisualStyleBackColor = true;
            this.chooseMultiImgBtn.Visible = false;
            this.chooseMultiImgBtn.Click += new System.EventHandler(this.ChooseMultiImg);
            // 
            // logBox
            // 
            this.logBox.BackColor = System.Drawing.Color.White;
            this.logBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.logBox.Location = new System.Drawing.Point(484, 559);
            this.logBox.Multiline = true;
            this.logBox.Name = "logBox";
            this.logBox.ReadOnly = true;
            this.logBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.logBox.Size = new System.Drawing.Size(85, 25);
            this.logBox.TabIndex = 31;
            this.logBox.Visible = false;
            // 
            // chooseImgBtn
            // 
            this.chooseImgBtn.Location = new System.Drawing.Point(482, 350);
            this.chooseImgBtn.Name = "chooseImgBtn";
            this.chooseImgBtn.Size = new System.Drawing.Size(86, 48);
            this.chooseImgBtn.TabIndex = 30;
            this.chooseImgBtn.Text = "选择识别图";
            this.chooseImgBtn.UseVisualStyleBackColor = true;
            this.chooseImgBtn.Visible = false;
            this.chooseImgBtn.Click += new System.EventHandler(this.ChooseImg);
            // 
            // imageLists
            // 
            this.imageLists.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageLists.ImageSize = new System.Drawing.Size(80, 80);
            this.imageLists.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // matchBtn
            // 
            this.matchBtn.Location = new System.Drawing.Point(482, 404);
            this.matchBtn.Name = "matchBtn";
            this.matchBtn.Size = new System.Drawing.Size(86, 48);
            this.matchBtn.TabIndex = 34;
            this.matchBtn.Text = "开始匹配";
            this.matchBtn.UseVisualStyleBackColor = true;
            this.matchBtn.Visible = false;
            this.matchBtn.Click += new System.EventHandler(this.matchBtn_Click);
            // 
            // btnClearFaceList
            // 
            this.btnClearFaceList.Location = new System.Drawing.Point(482, 458);
            this.btnClearFaceList.Name = "btnClearFaceList";
            this.btnClearFaceList.Size = new System.Drawing.Size(86, 48);
            this.btnClearFaceList.TabIndex = 35;
            this.btnClearFaceList.Text = "清空人脸库";
            this.btnClearFaceList.UseVisualStyleBackColor = true;
            this.btnClearFaceList.Visible = false;
            this.btnClearFaceList.Click += new System.EventHandler(this.btnClearFaceList_Click);
            // 
            // lblCompareImage
            // 
            this.lblCompareImage.AutoSize = true;
            this.lblCompareImage.BackColor = System.Drawing.Color.Transparent;
            this.lblCompareImage.ForeColor = System.Drawing.Color.White;
            this.lblCompareImage.Location = new System.Drawing.Point(479, 294);
            this.lblCompareImage.Name = "lblCompareImage";
            this.lblCompareImage.Size = new System.Drawing.Size(59, 12);
            this.lblCompareImage.TabIndex = 36;
            this.lblCompareImage.Text = "比对人脸:";
            this.lblCompareImage.Visible = false;
            this.lblCompareImage.Click += new System.EventHandler(this.lblCompareImage_Click);
            // 
            // lblCompareInfo
            // 
            this.lblCompareInfo.AutoSize = true;
            this.lblCompareInfo.BackColor = System.Drawing.Color.Transparent;
            this.lblCompareInfo.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblCompareInfo.ForeColor = System.Drawing.Color.White;
            this.lblCompareInfo.Location = new System.Drawing.Point(544, 290);
            this.lblCompareInfo.Name = "lblCompareInfo";
            this.lblCompareInfo.Size = new System.Drawing.Size(17, 16);
            this.lblCompareInfo.TabIndex = 37;
            this.lblCompareInfo.Text = "-";
            this.lblCompareInfo.Visible = false;
            // 
            // rgbVideoSource
            // 
            this.rgbVideoSource.BackColor = System.Drawing.Color.White;
            this.rgbVideoSource.BorderColor = System.Drawing.Color.Transparent;
            this.rgbVideoSource.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rgbVideoSource.ForeColor = System.Drawing.Color.Transparent;
            this.rgbVideoSource.Location = new System.Drawing.Point(0, 0);
            this.rgbVideoSource.Margin = new System.Windows.Forms.Padding(0);
            this.rgbVideoSource.Name = "rgbVideoSource";
            this.rgbVideoSource.Size = new System.Drawing.Size(576, 756);
            this.rgbVideoSource.TabIndex = 38;
            this.rgbVideoSource.Text = "videoSource";
            this.rgbVideoSource.VideoSource = null;
            this.rgbVideoSource.PlayingFinished += new AForge.Video.PlayingFinishedEventHandler(this.videoSource_PlayingFinished);
            this.rgbVideoSource.Click += new System.EventHandler(this.rgbVideoSource_Click);
            this.rgbVideoSource.Paint += new System.Windows.Forms.PaintEventHandler(this.videoSource_Paint);
            // 
            // btnStartVideo
            // 
            this.btnStartVideo.Location = new System.Drawing.Point(481, 509);
            this.btnStartVideo.Name = "btnStartVideo";
            this.btnStartVideo.Size = new System.Drawing.Size(89, 49);
            this.btnStartVideo.TabIndex = 39;
            this.btnStartVideo.Text = "启用摄像头";
            this.btnStartVideo.UseVisualStyleBackColor = true;
            this.btnStartVideo.Visible = false;
            this.btnStartVideo.Click += new System.EventHandler(this.btnStartVideo_Click);
            // 
            // txtThreshold
            // 
            this.txtThreshold.BackColor = System.Drawing.SystemColors.Window;
            this.txtThreshold.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtThreshold.Location = new System.Drawing.Point(529, 319);
            this.txtThreshold.Name = "txtThreshold";
            this.txtThreshold.Size = new System.Drawing.Size(39, 25);
            this.txtThreshold.TabIndex = 40;
            this.txtThreshold.Text = "0.8";
            this.txtThreshold.Visible = false;
            this.txtThreshold.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtThreshold_KeyPress);
            this.txtThreshold.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtThreshold_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(479, 323);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 16);
            this.label1.TabIndex = 41;
            this.label1.Text = "阈值:";
            this.label1.Visible = false;
            // 
            // irVideoSource
            // 
            this.irVideoSource.BackColor = System.Drawing.SystemColors.Control;
            this.irVideoSource.Location = new System.Drawing.Point(481, 179);
            this.irVideoSource.Name = "irVideoSource";
            this.irVideoSource.Size = new System.Drawing.Size(85, 25);
            this.irVideoSource.TabIndex = 38;
            this.irVideoSource.Text = "videoSource";
            this.irVideoSource.VideoSource = null;
            this.irVideoSource.Visible = false;
            this.irVideoSource.PlayingFinished += new AForge.Video.PlayingFinishedEventHandler(this.videoSource_PlayingFinished);
            this.irVideoSource.Paint += new System.Windows.Forms.PaintEventHandler(this.irVideoSource_Paint);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(260, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(305, 34);
            this.label3.TabIndex = 44;
            this.label3.Text = "yyyy年MM月dd日 星期W";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(233, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(193, 38);
            this.label2.TabIndex = 43;
            this.label2.Text = "让视界从此不同";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            this.label2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label2_MouseDown);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("宋体", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(482, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(222, 55);
            this.label4.TabIndex = 42;
            this.label4.Text = "人脸考勤";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label4.Visible = false;
            // 
            // pictureBox_close
            // 
            this.pictureBox_close.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_close.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_close.Image")));
            this.pictureBox_close.Location = new System.Drawing.Point(51, 20);
            this.pictureBox_close.Name = "pictureBox_close";
            this.pictureBox_close.Size = new System.Drawing.Size(34, 38);
            this.pictureBox_close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_close.TabIndex = 45;
            this.pictureBox_close.TabStop = false;
            this.pictureBox_close.Click += new System.EventHandler(this.pictureBox_close_Click);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("宋体", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(482, 235);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 39);
            this.label5.TabIndex = 46;
            this.label5.Text = "人脸库";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label5.Visible = false;
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // updateTime
            // 
            this.updateTime.Enabled = true;
            this.updateTime.Interval = 1000;
            this.updateTime.Tick += new System.EventHandler(this.updateTime_Tick);
            // 
            // imageList
            // 
            this.imageList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.imageList.LargeImageList = this.imageLists;
            this.imageList.Location = new System.Drawing.Point(484, 677);
            this.imageList.Name = "imageList";
            this.imageList.Size = new System.Drawing.Size(21, 10);
            this.imageList.TabIndex = 33;
            this.imageList.UseCompatibleStateImageBehavior = false;
            this.imageList.SelectedIndexChanged += new System.EventHandler(this.imageList_SelectedIndexChanged);
            this.imageList.DoubleClick += new System.EventHandler(this.imageList_DoubleClick);
            this.imageList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.imageList_MouseDoubleClick);
            // 
            // lab_hhmm
            // 
            this.lab_hhmm.BackColor = System.Drawing.Color.Transparent;
            this.lab_hhmm.Font = new System.Drawing.Font("宋体", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_hhmm.ForeColor = System.Drawing.Color.Transparent;
            this.lab_hhmm.Location = new System.Drawing.Point(80, 5);
            this.lab_hhmm.Name = "lab_hhmm";
            this.lab_hhmm.Size = new System.Drawing.Size(169, 50);
            this.lab_hhmm.TabIndex = 47;
            this.lab_hhmm.Text = "HH:mm:ss";
            this.lab_hhmm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.pic_logo);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(576, 82);
            this.panel1.TabIndex = 48;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.pic_max);
            this.panel3.Controls.Add(this.pictureBox_close);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(484, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(92, 82);
            this.panel3.TabIndex = 50;
            this.panel3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel3_MouseDown);
            // 
            // pic_max
            // 
            this.pic_max.BackColor = System.Drawing.Color.Transparent;
            this.pic_max.Image = ((System.Drawing.Image)(resources.GetObject("pic_max.Image")));
            this.pic_max.Location = new System.Drawing.Point(9, 20);
            this.pic_max.Name = "pic_max";
            this.pic_max.Size = new System.Drawing.Size(34, 40);
            this.pic_max.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_max.TabIndex = 49;
            this.pic_max.TabStop = false;
            this.pic_max.Click += new System.EventHandler(this.pic_max_Click);
            // 
            // pic_logo
            // 
            this.pic_logo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pic_logo.BackgroundImage")));
            this.pic_logo.Location = new System.Drawing.Point(0, 0);
            this.pic_logo.Name = "pic_logo";
            this.pic_logo.Size = new System.Drawing.Size(236, 82);
            this.pic_logo.TabIndex = 44;
            this.pic_logo.TabStop = false;
            this.pic_logo.Click += new System.EventHandler(this.pic_logo_Click);
            this.pic_logo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic_logo_MouseDown);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.pic_set);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.lab_hhmm);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 700);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(576, 56);
            this.panel2.TabIndex = 49;
            // 
            // pic_set
            // 
            this.pic_set.BackColor = System.Drawing.Color.Transparent;
            this.pic_set.Image = ((System.Drawing.Image)(resources.GetObject("pic_set.Image")));
            this.pic_set.Location = new System.Drawing.Point(7, 10);
            this.pic_set.Name = "pic_set";
            this.pic_set.Size = new System.Drawing.Size(35, 36);
            this.pic_set.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_set.TabIndex = 48;
            this.pic_set.TabStop = false;
            this.pic_set.Click += new System.EventHandler(this.pic_set_Click);
            // 
            // pan_kq
            // 
            this.pan_kq.BackColor = System.Drawing.Color.Transparent;
            this.pan_kq.Controls.Add(this.pic_facex);
            this.pan_kq.Controls.Add(this.lab_date);
            this.pan_kq.Controls.Add(this.pic_face);
            this.pan_kq.Controls.Add(this.lab_name);
            this.pan_kq.Controls.Add(this.lab_dept);
            this.pan_kq.Location = new System.Drawing.Point(0, 82);
            this.pan_kq.Name = "pan_kq";
            this.pan_kq.Size = new System.Drawing.Size(212, 247);
            this.pan_kq.TabIndex = 50;
            // 
            // pic_facex
            // 
            this.pic_facex.BackColor = System.Drawing.Color.White;
            this.pic_facex.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pic_facex.ErrorImage")));
            this.pic_facex.InitialImage = ((System.Drawing.Image)(resources.GetObject("pic_facex.InitialImage")));
            this.pic_facex.Location = new System.Drawing.Point(30, 13);
            this.pic_facex.Name = "pic_facex";
            this.pic_facex.Size = new System.Drawing.Size(159, 142);
            this.pic_facex.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic_facex.TabIndex = 54;
            this.pic_facex.TabStop = false;
            // 
            // lab_date
            // 
            this.lab_date.BackColor = System.Drawing.Color.Transparent;
            this.lab_date.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_date.ForeColor = System.Drawing.Color.White;
            this.lab_date.Location = new System.Drawing.Point(-1, 212);
            this.lab_date.Name = "lab_date";
            this.lab_date.Size = new System.Drawing.Size(213, 38);
            this.lab_date.TabIndex = 52;
            this.lab_date.Text = "考勤时间";
            this.lab_date.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pic_face
            // 
            this.pic_face.Location = new System.Drawing.Point(12, 13);
            this.pic_face.Name = "pic_face";
            this.pic_face.Size = new System.Drawing.Size(18, 25);
            this.pic_face.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic_face.TabIndex = 0;
            this.pic_face.TabStop = false;
            this.pic_face.Visible = false;
            // 
            // lab_name
            // 
            this.lab_name.BackColor = System.Drawing.Color.Transparent;
            this.lab_name.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_name.ForeColor = System.Drawing.Color.White;
            this.lab_name.Location = new System.Drawing.Point(2, 183);
            this.lab_name.Name = "lab_name";
            this.lab_name.Size = new System.Drawing.Size(210, 38);
            this.lab_name.TabIndex = 51;
            this.lab_name.Text = "员工姓名";
            this.lab_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab_dept
            // 
            this.lab_dept.BackColor = System.Drawing.Color.Transparent;
            this.lab_dept.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_dept.ForeColor = System.Drawing.Color.White;
            this.lab_dept.Location = new System.Drawing.Point(1, 154);
            this.lab_dept.Name = "lab_dept";
            this.lab_dept.Size = new System.Drawing.Size(211, 38);
            this.lab_dept.TabIndex = 53;
            this.lab_dept.Text = "部门";
            this.lab_dept.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FaceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(576, 756);
            this.Controls.Add(this.pan_kq);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.irVideoSource);
            this.Controls.Add(this.matchBtn);
            this.Controls.Add(this.btnStartVideo);
            this.Controls.Add(this.lblCompareImage);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblCompareInfo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.logBox);
            this.Controls.Add(this.picImageCompare);
            this.Controls.Add(this.txtThreshold);
            this.Controls.Add(this.chooseImgBtn);
            this.Controls.Add(this.btnClearFaceList);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.chooseMultiImgBtn);
            this.Controls.Add(this.imageList);
            this.Controls.Add(this.rgbVideoSource);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FaceForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ArcSoftFace C# demo";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Closed);
            this.Load += new System.EventHandler(this.FaceForm_Load);
            this.Click += new System.EventHandler(this.FaceForm_Click);
            this.DoubleClick += new System.EventHandler(this.FaceForm_DoubleClick);
            ((System.ComponentModel.ISupportInitialize)(this.picImageCompare)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_close)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_logo)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_set)).EndInit();
            this.pan_kq.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_facex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_face)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picImageCompare;
        private System.Windows.Forms.Button chooseMultiImgBtn;
        private System.Windows.Forms.TextBox logBox;
        private System.Windows.Forms.Button chooseImgBtn;
        private System.Windows.Forms.ImageList imageLists;
        private System.Windows.Forms.Button matchBtn;
        private System.Windows.Forms.Button btnClearFaceList;
        private System.Windows.Forms.Label lblCompareImage;
        private System.Windows.Forms.Label lblCompareInfo;
        private AForge.Controls.VideoSourcePlayer rgbVideoSource;
        private System.Windows.Forms.Button btnStartVideo;
        private System.Windows.Forms.TextBox txtThreshold;
        private System.Windows.Forms.Label label1;
        private AForge.Controls.VideoSourcePlayer irVideoSource;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox_close;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.Timer updateTime;
        private System.Windows.Forms.ListView imageList;
        private System.Windows.Forms.Label lab_hhmm;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pic_logo;
        private System.Windows.Forms.PictureBox pic_set;
        private System.Windows.Forms.PictureBox pic_max;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel pan_kq;
        private System.Windows.Forms.Label lab_date;
        private System.Windows.Forms.Label lab_name;
        private System.Windows.Forms.PictureBox pic_face;
        private System.Windows.Forms.Label lab_dept;
        private iControls.CirclePictureBox pic_facex;
    }
}

