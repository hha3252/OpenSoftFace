using System;

namespace CommonConstant
{
	public class SwitchStateConst
	{
		public static string TRUE_VALUE = "1";

		public static string FALSE_VALUE = "0";
	}
}
