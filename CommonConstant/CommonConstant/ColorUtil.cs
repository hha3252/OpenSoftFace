using System;
using System.Drawing;

namespace CommonConstant
{
	public class ColorUtil
	{
		public static Color Transparent = Color.Transparent;

		public static Color White = Color.White;

		public static Color WhiteSmoke = Color.FromArgb(248, 248, 248);

		public static Color Blue = Color.FromArgb(1, 119, 213);

		public static Color DarkBlue = Color.FromArgb(19, 144, 229);

		public static Color DoderBlue = Color.FromArgb(158, 212, 255);

		public static Color LightSkyBlue = Color.FromArgb(211, 235, 254);

		public static Color AirBlue = Color.FromArgb(51, 94, 168);

		public static Color SlateGray = Color.FromArgb(220, 220, 220);

		public static Color LightSlateGray = Color.FromArgb(244, 244, 244);

		public static Color DarkGray = Color.DarkGray;

		public static Color SoftGray = Color.FromArgb(239, 240, 241);

		public static Color Gray = Color.Gray;

		public static Color Black = Color.Black;

		public static Color SoftBlack = Color.FromArgb(40, 43, 51);

		public static Color DarkBlack = Color.FromArgb(59, 63, 73);

		public static Color LightBlack = Color.FromArgb(52, 52, 52);

		public static Color Yellow = Color.Yellow;

		public static Color Silver = Color.Silver;

		public static Color Beige = Color.FromArgb(236, 233, 216);

		public static Color DeepBlue = Color.FromArgb(1, 119, 213);

		public static Color TabBack = Color.FromArgb(37, 52, 69);

		public static Color TabSelectFore = Color.FromArgb(15, 229, 234);

		public static Color GridViewTitleForeColor = Color.FromArgb(195, 228, 250);
	}
}
