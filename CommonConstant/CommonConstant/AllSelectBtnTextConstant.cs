using System;

namespace CommonConstant
{
	public class AllSelectBtnTextConstant
	{
		public const string SELECTALL_TEXT = "全选";

		public const string UNSELECTALL_TEXT = "取消全选";
	}
}
