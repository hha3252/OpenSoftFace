using LogUtil;
using System;
using System.IO;

namespace CommonConstant
{
	public class AppConfig
	{
        public static string CONFIG_DIR = System.Environment.CurrentDirectory + "\\OpenSoftFaceFile\\";

        public const string IMAGE_DIR = "OpenSoftFaceFile\\";

		public const string PERSON_IMAGE_TEMP_DIR = "\\temp\\";

		public const string PERSON_IMAGE_SOURCE_DIR = "\\source\\";

		public const string PERSON_IMAGE_SHOW_DIR = "\\show\\";

		public const string CONFIG_FILE_NAME = "setting.ini";

		private static AppConfig Instance = null;

		public string ParentDir
		{
			get;
			set;
		}

		private AppConfig()
		{
			try
			{
				if (!Directory.Exists(AppConfig.CONFIG_DIR))
				{
					Directory.CreateDirectory(AppConfig.CONFIG_DIR);
				}
				if (!File.Exists(AppConfig.CONFIG_DIR + "setting.ini"))
				{
					File.Create(AppConfig.CONFIG_DIR + "setting.ini").Close();
				}
				try
				{
					this.ParentDir = Directory.GetParent(Environment.CurrentDirectory).FullName + "\\";
				}
				catch (Exception ex)
				{
					LogHelper.LogError(base.GetType(), ex);
					File.Create(AppConfig.CONFIG_DIR + "setting.ini").Close();
				}
			}
			catch (Exception ee)
			{
				LogHelper.LogError(typeof(AppConfig), ee);
			}
		}

		public static AppConfig GetInstance()
		{
			if (AppConfig.Instance == null)
			{
				AppConfig.Instance = new AppConfig();
			}
			return AppConfig.Instance;
		}
	}
}
