using System;

namespace CommonConstant
{
	public class FormState
	{
		public const string NORMAL = "还原";

		public const string MAXIMIZED = "最大化";

		public const string WARNING_FORM_TITLE = "";

		public const string ERROR_FORM_TITLE = "";
	}
}
