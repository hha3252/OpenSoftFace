using System;

namespace CommonConstant
{
	public class ShowVoice
	{
		public const string VOICE_NONE = "0";

		public const string VOICE_RECOG_SUCCESS = "1";

		public const string VOICE_RECOG_SUCCESS_TEXT = "识别成功";

		public const string VOICE_CHECK_SUCCESS = "2";

		public const string VOICE_CHECK_SUCCESS_TEXT = "打卡成功";
	}
}
