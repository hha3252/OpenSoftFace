using System;

namespace CommonConstant
{
	public class RuleEffectiveMode
	{
		public const int RIGHT_NOW = 0;

		public const int NEXT_MONTH = 1;
	}
}
