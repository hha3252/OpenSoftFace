using System;

namespace CommonConstant
{
	public class RestDayConstant
	{
		public const string MONDY = "1";

		public const string TUESDAY = "2";

		public const string WEDNESDAY = "3";

		public const string THURSDAY = "4";

		public const string FRIDAY = "5";

		public const string SATURDAY = "6";

		public const string SUNDAY = "7";
	}
}
