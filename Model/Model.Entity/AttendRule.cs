using System;

namespace Model.Entity
{
	public class AttendRule
	{
		public int ID
		{
			get;
			set;
		}

		public string Json
		{
			get;
			set;
		}

		public string StartDate
		{
			get;
			set;
		}

		public string EndDate
		{
			get;
			set;
		}

		public AttendRuleParam attendRuleParam
		{
			get;
			set;
		}
	}
}
