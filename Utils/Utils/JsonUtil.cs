using LitJson2;
using LogUtil;
using System;

namespace Utils
{
	public class JsonUtil
	{
		public static object getJsonValueByKey(JsonData jsonData, string key, object defaultValue)
		{
			object result = null;
			if (defaultValue != null)
			{
				result = defaultValue;
			}
			if (jsonData == null || string.IsNullOrWhiteSpace(key))
			{
				return result;
			}
			try
			{
				result = jsonData[key];
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(JsonUtil), se);
			}
			return result;
		}
	}
}
