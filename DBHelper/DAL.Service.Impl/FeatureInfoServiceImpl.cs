using CommonConstant;
using LogUtil;
using Model.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Text;
using Utils;

namespace DAL.Service.Impl
{
	public class FeatureInfoServiceImpl : IFeatureInfoService
	{
		private string connectionString;

		public FeatureInfoServiceImpl()
		{
			try
			{
				this.connectionString = new SQLiteConnectionStringBuilder
				{
					DataSource = DBConstant.DB_DIR + "generalattendance.db"
				}.ToString();
			}
			catch (Exception ex)
			{
				LogHelper.LogError(base.GetType(), ex);
			}
		}

		public int Count(string[] where = null, bool distinct = false)
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					StringBuilder sqlBuilder = new StringBuilder();
					if (distinct)
					{
						sqlBuilder.Append(string.Format(" select distinct count(*) as num from {0} ", "FeatureInfo"));
					}
					else
					{
						sqlBuilder.Append(string.Format(" select count(*) as num from {0} ", "FeatureInfo"));
					}
					if (where != null && where.Length != 0)
					{
						sqlBuilder.Append(" where ");
						for (int i = 0; i < where.Length; i++)
						{
							if (i == 0)
							{
								sqlBuilder.Append(string.Format(" {0} ", where[i]));
							}
							else
							{
								sqlBuilder.Append(string.Format(" and {0} ", where[i]));
							}
						}
					}
					Console.WriteLine("sql:{0}", sqlBuilder.ToString());
					using (SQLiteCommand command = conn.CreateCommand())
					{
						command.CommandText = sqlBuilder.ToString();
						SQLiteDataReader reader = command.ExecuteReader();
						int num = 0;
						if (reader.Read())
						{
							num = int.Parse(reader["num"].ToString());
						}
						reader.Close();
						command.Dispose();
						conn.Close();
						return num;
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return 0;
		}

		public int Delete(int faceInfoID)
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				conn.SetPassword(ComputerUtil.GetDBPasswordStr());
				conn.Open();
				try
				{
					string sql = string.Format("delete from {0} where person_id={1} and f_valid > 0", "FeatureInfo", faceInfoID);
					Console.WriteLine("sql:{0}", sql);
					object dBLocker = DBHelper.DBLocker;
					lock (dBLocker)
					{
						using (SQLiteCommand command = conn.CreateCommand())
						{
							command.CommandText = sql;
							int arg_72_0 = command.ExecuteNonQuery();
							command.Dispose();
							conn.Close();
							return arg_72_0;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return 0;
		}

		public FeatureInfo Get(int faceInfoID)
		{
			FeatureInfo result;
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					string sql = string.Format("select * from {0} where person_id={1} and f_valid > 0", "FeatureInfo", faceInfoID);
					Console.WriteLine("sql:{0}", sql);
					using (SQLiteCommand command = conn.CreateCommand())
					{
						command.CommandText = sql;
						SQLiteDataReader reader = command.ExecuteReader();
						FeatureInfo faceInfo = null;
						if (reader.Read())
						{
							faceInfo = new FeatureInfo();
							faceInfo.ID = StringUtil.CheckSqlIntValue(reader["id"].ToString());
							faceInfo.PersonId = StringUtil.CheckSqlIntValue(reader["person_id"].ToString());
							faceInfo.FaceId = StringUtil.CheckSqlIntValue(reader["face_id"].ToString());
							faceInfo.RegisImageName = reader["regis_image_name"].ToString();
							faceInfo.FLeft = StringUtil.CheckSqlIntValue(reader["f_left"].ToString());
							faceInfo.FTop = StringUtil.CheckSqlIntValue(reader["f_top"].ToString());
							faceInfo.FRight = StringUtil.CheckSqlIntValue(reader["f_right"].ToString());
							faceInfo.FBottom = StringUtil.CheckSqlIntValue(reader["f_bottom"].ToString());
							faceInfo.FOrient = StringUtil.CheckSqlIntValue(reader["f_orient"].ToString());
							faceInfo.FValid = StringUtil.CheckSqlIntValue(reader["f_valid"].ToString());
							faceInfo.FeatureSize = StringUtil.CheckSqlIntValue(reader["feature_size"].ToString());
						}
						reader.Close();
						command.Dispose();
						conn.Close();
						result = faceInfo;
						return result;
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
				result = null;
			}
			return result;
		}

		public int Insert(FeatureInfo faceInfo)
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				conn.SetPassword(ComputerUtil.GetDBPasswordStr());
				conn.Open();
				try
				{
					string sql = string.Format("insert into {0} values(null,{1},{2},'{3}',{4},{5},{6},{7},{8},{9},@feature,{10})", new object[]
					{
						"FeatureInfo",
						faceInfo.PersonId,
						faceInfo.FaceId,
						faceInfo.RegisImageName,
						faceInfo.FLeft,
						faceInfo.FTop,
						faceInfo.FRight,
						faceInfo.FBottom,
						faceInfo.FOrient,
						faceInfo.FValid,
						faceInfo.FeatureSize
					});
					object dBLocker = DBHelper.DBLocker;
					lock (dBLocker)
					{
						using (SQLiteCommand command = conn.CreateCommand())
						{
							command.CommandText = sql;
							command.Parameters.Add("@feature", DbType.Binary).Value = faceInfo.Feature;
							int arg_111_0 = command.ExecuteNonQuery();
							command.Dispose();
							conn.Close();
							return arg_111_0;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return 0;
		}

		public List<FeatureInfo> QueryForList(string[] where = null, string groupBy = null, string having = null, string orderBy = null, int offset = -1, int size = -1)
		{
			List<FeatureInfo> list = new List<FeatureInfo>();
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					StringBuilder sqlBuilder = new StringBuilder();
					sqlBuilder.Append(string.Format(" select * from {0} ", "FeatureInfo"));
					if (where != null && where.Length != 0)
					{
						sqlBuilder.Append(string.Format(" where ", new object[0]));
						for (int i = 0; i < where.Length; i++)
						{
							if (i == 0)
							{
								sqlBuilder.Append(string.Format(" {0} ", where[i]));
							}
							else
							{
								sqlBuilder.Append(string.Format(" and {0} ", where[i]));
							}
						}
					}
					if (groupBy != null && groupBy != string.Empty)
					{
						sqlBuilder.Append(string.Format(" group by {0} ", groupBy));
					}
					if (having != null && having != string.Empty)
					{
						sqlBuilder.Append(string.Format(" having {0} ", having));
					}
					if (orderBy != null && orderBy != string.Empty)
					{
						sqlBuilder.Append(string.Format(" order by {0} ", orderBy));
					}
					if (offset >= 0 && size >= 0)
					{
						sqlBuilder.Append(string.Format(" limit {0},{1} ", offset, size));
					}
					Console.WriteLine("sql:{0}", sqlBuilder.ToString());
					using (SQLiteCommand command = conn.CreateCommand())
					{
						command.CommandText = sqlBuilder.ToString();
						SQLiteDataReader reader = command.ExecuteReader();
						while (reader.Read())
						{
							list.Add(new FeatureInfo
							{
								ID = StringUtil.CheckSqlIntValue(reader["id"].ToString()),
								PersonId = StringUtil.CheckSqlIntValue(reader["person_id"].ToString()),
								FaceId = StringUtil.CheckSqlIntValue(reader["face_id"].ToString()),
								RegisImageName = reader["regis_image_name"].ToString(),
								FLeft = StringUtil.CheckSqlIntValue(reader["f_left"].ToString()),
								FTop = StringUtil.CheckSqlIntValue(reader["f_top"].ToString()),
								FRight = StringUtil.CheckSqlIntValue(reader["f_right"].ToString()),
								FBottom = StringUtil.CheckSqlIntValue(reader["f_bottom"].ToString()),
								FOrient = StringUtil.CheckSqlIntValue(reader["f_orient"].ToString()),
								FValid = StringUtil.CheckSqlIntValue(reader["f_valid"].ToString()),
								FeatureSize = 1032
							});
						}
						reader.Close();
						command.Dispose();
						conn.Close();
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return list;
		}

		public List<FeatureInfo> QueryForListJoin(string[] where = null, string groupBy = null, string having = null, string orderBy = null, int offset = -1, int size = -1)
		{
			List<FeatureInfo> list = new List<FeatureInfo>();
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					StringBuilder sqlBuilder = new StringBuilder();
					sqlBuilder.Append(string.Format(" select * from {0} faceinfo left join {1} personinfo on faceinfo.person_id=personinfo.id ", "FeatureInfo", "MainInfo"));
					if (where != null && where.Length != 0)
					{
						sqlBuilder.Append(string.Format(" where ", new object[0]));
						for (int i = 0; i < where.Length; i++)
						{
							if (i == 0)
							{
								sqlBuilder.Append(string.Format(" {0} ", where[i]));
							}
							else
							{
								sqlBuilder.Append(string.Format(" and {0} ", where[i]));
							}
						}
					}
					if (groupBy != null && groupBy != string.Empty)
					{
						sqlBuilder.Append(string.Format(" group by {0} ", groupBy));
					}
					if (having != null && having != string.Empty)
					{
						sqlBuilder.Append(string.Format(" having {0} ", having));
					}
					if (orderBy != null && orderBy != string.Empty)
					{
						sqlBuilder.Append(string.Format(" order by {0} ", orderBy));
					}
					if (offset >= 0 && size >= 0)
					{
						sqlBuilder.Append(string.Format(" limit {0},{1} ", offset, size));
					}
					Console.WriteLine("sql:{0}", sqlBuilder.ToString());
					using (SQLiteCommand command = conn.CreateCommand())
					{
						command.CommandText = sqlBuilder.ToString();
						SQLiteDataReader reader = command.ExecuteReader();
						while (reader.Read())
						{
							list.Add(new FeatureInfo
							{
								ID = StringUtil.CheckSqlIntValue(reader["id"].ToString()),
								PersonId = StringUtil.CheckSqlIntValue(reader["person_id"].ToString()),
								FaceId = StringUtil.CheckSqlIntValue(reader["face_id"].ToString()),
								RegisImageName = reader["regis_image_name"].ToString(),
								FLeft = StringUtil.CheckSqlIntValue(reader["f_left"].ToString()),
								FTop = StringUtil.CheckSqlIntValue(reader["f_top"].ToString()),
								FRight = StringUtil.CheckSqlIntValue(reader["f_right"].ToString()),
								FBottom = StringUtil.CheckSqlIntValue(reader["f_bottom"].ToString()),
								FOrient = StringUtil.CheckSqlIntValue(reader["f_orient"].ToString()),
								FValid = StringUtil.CheckSqlIntValue(reader["f_valid"].ToString()),
								FeatureSize = StringUtil.CheckSqlIntValue(reader["feature_size"].ToString())
							});
						}
						reader.Close();
						command.Dispose();
						conn.Close();
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return list;
		}

		public int Update(FeatureInfo faceInfo)
		{
			int result;
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				conn.SetPassword(ComputerUtil.GetDBPasswordStr());
				conn.Open();
				try
				{
					string sql = string.Format("update {0} set person_id={1},face_id={2},regis_image_name='{3}',f_left={4},f_top={5},f_right={6},f_bottom={7},f_orient={8},f_valid={9},feature=@feature,feature_size={10} where id={11}", new object[]
					{
						"FeatureInfo",
						faceInfo.PersonId,
						faceInfo.FaceId,
						faceInfo.RegisImageName,
						faceInfo.FLeft,
						faceInfo.FTop,
						faceInfo.FRight,
						faceInfo.FBottom,
						faceInfo.FOrient,
						faceInfo.FValid,
						faceInfo.FeatureSize,
						faceInfo.ID
					});
					Console.WriteLine("sql:{0}", sql);
					object dBLocker = DBHelper.DBLocker;
					lock (dBLocker)
					{
						using (SQLiteCommand command = conn.CreateCommand())
						{
							command.CommandText = sql;
							command.Parameters.Add("@feature", DbType.Binary).Value = faceInfo.Feature;
							int arg_12B_0 = command.ExecuteNonQuery();
							command.Dispose();
							conn.Close();
							result = arg_12B_0;
							return result;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
				result = 0;
			}
			return result;
		}

		private static byte[] GetBytes(SQLiteDataReader reader)
		{
			byte[] buffer = new byte[1032];
			try
			{
				long fieldOffset = 0L;
				using (MemoryStream stream = new MemoryStream())
				{
					long bytesRead;
					while ((bytesRead = reader.GetBytes(0, fieldOffset, buffer, 0, buffer.Length)) > 0L)
					{
						stream.Write(buffer, 0, (int)bytesRead);
						fieldOffset += bytesRead;
					}
					return stream.ToArray();
				}
			}
			catch
			{
			}
			return buffer;
		}

		public int Update(List<int> idList, int status)
		{
			if (idList == null || idList.Count <= 0)
			{
				return 0;
			}
			int result;
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				conn.SetPassword(ComputerUtil.GetDBPasswordStr());
				conn.Open();
				try
				{
					StringBuilder sbStr = new StringBuilder();
					for (int i = 0; i < idList.Count; i++)
					{
						if (i != 0)
						{
							sbStr.Append(",");
						}
						StringBuilder arg_55_0 = sbStr;
						result = idList[i];
						arg_55_0.Append(result.ToString());
					}
					string sql = string.Format("update {0} set f_valid={1} where id in({2})", "FeatureInfo", status, sbStr.ToString());
					object dBLocker = DBHelper.DBLocker;
					lock (dBLocker)
					{
						using (SQLiteCommand command = conn.CreateCommand())
						{
							command.CommandText = sql;
							int arg_BB_0 = command.ExecuteNonQuery();
							command.Dispose();
							conn.Close();
							result = arg_BB_0;
							return result;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
				result = 0;
			}
			return result;
		}
	}
}
